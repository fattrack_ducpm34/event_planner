﻿using AutoMapper;
using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Core.Request;
using EventPlannerServices.Core.Responses;
using EventPlannerServices.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace EventPlannerServices.Infrastructure.Services
{
    public class BookingProvider : IBookingProvider
    {
        private readonly ILogger<BookingProvider> _logger;
        private readonly BookMeDbContext _dbContext;
        private readonly IMapper _mapper;

        public BookingProvider(ILogger<BookingProvider> logger, BookMeDbContext dbContext, IMapper mapper)
        {
            _logger = logger;
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public string AddBooking(int userId, int roomId, DateTime startTime, DateTime endTime, string bookingName, string bookingDescription, string AttendeesList)
        {
            // The timeframe is not in the past
            if (DateTime.Now > startTime || DateTime.Now > endTime || (startTime > endTime))
            {
                _logger.LogError("Invalid timeframe");
                throw new ArgumentException("Invalid timeframe");
            }

            List<string> result = _dbContext.Database.SqlQuery<string>($"InsertBooking {userId}, {roomId}, {startTime}, {endTime}, {bookingName}, {bookingDescription}, {AttendeesList}").ToList();
            return result.Count() == 0 ? string.Empty : result[0];
        }

        public async Task<Booking> UpdateReservation(int bookingId, int userId, DateTime startTime, DateTime endTime, string bookingName, string bookingDescription, string AttendeesList)
        {
            var booking = await _dbContext.Bookings
                .Where(x => x.Id == bookingId && x.UserId == userId)
                .Include(x => x.User)
                .Include(x => x.Room)
                .FirstOrDefaultAsync();

            if (DateTime.Now > startTime || DateTime.Now > endTime || (startTime > endTime) || (booking.StartTime - DateTime.Now).TotalMinutes < 30)
            {
                _logger.LogError("Invalid timeframe or less than 30 minutes before start.");
                throw new ArgumentException("Invalid timeframe or less than 30 minutes before start.");
            }

            var checkConflictTimeframe = await _dbContext.Bookings.Where(x => x.RoomId == booking.RoomId && x.Id != bookingId && x.StartTime.Date == startTime.Date && x.EndTime.Date == endTime.Date && x.StartTime.TimeOfDay < endTime.TimeOfDay && x.EndTime.TimeOfDay > startTime.TimeOfDay).AnyAsync();

            if (checkConflictTimeframe)
            {
                _logger.LogError("Invalid time frame. This timeframe has been booked for this room");
                throw new ArgumentException("Invalid time frame. This timeframe has been booked for this room");
            }

            booking.LastModifiedDate = DateTime.Now;
            booking.StartTime = startTime;
            booking.EndTime = endTime;
            booking.Name = bookingName;
            booking.Description = bookingDescription;
            booking.AttendeesList = AttendeesList;

            _dbContext.Update(booking);
            await _dbContext.SaveChangesAsync();

            return booking;
        }

        public async Task<Booking> CancelReservation(int bookingId, int userId)
        {
            var booking = await _dbContext.Bookings
                .Where(x => x.Id == bookingId && x.UserId == userId)
                .Include(x => x.User)
                .Include(x => x.Room)
                .FirstOrDefaultAsync();

            if (DateTime.Now > booking.StartTime || (booking.StartTime - DateTime.Now).TotalMinutes < 30)
            {
                _logger.LogError("Reservation is  in the past or less than 30 minutes before start.");
                throw new ArgumentException("Reservation is  in the past or less than 30 minutes before start.");
            }

            if (booking != null)
            {
                booking.IsDelete = true;
            }

            _dbContext.Update(booking);
            await _dbContext.SaveChangesAsync();

            return booking;
        }

        /// <summary>
        /// TODO: This method is not good. We should refactor it.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<List<BookingInfoResponse>> GetBookingsInfo(GetBookingInfoRequest request)
        {
            var result = await _dbContext.Rooms
                .Include(room => room.Bookings)
                .Include(room => room.Status)
                .Include(x => x.ImageMaps)
                .ThenInclude(x => x.Image)
                .GroupBy(room => room.Location)
                .Where(group => group.Key == request.Location || string.IsNullOrWhiteSpace(request.Location))
                .Select(group => new BookingInfoResponse
                {
                    Location = group.Key,
                    Rooms = group
                    .Where(room => string.IsNullOrWhiteSpace(request.RoomName) || room.Name == request.RoomName)
                    .Where(room => string.IsNullOrWhiteSpace(request.RoomTypeId) || room.RoomTypeId == Int32.Parse(request.RoomTypeId))
                    .Where(room => string.IsNullOrWhiteSpace(request.Capacity) || room.Capacity >= Int32.Parse(request.Capacity))
                    .Select(room => new RoomInfo
                    {
                        Id = room.Id,
                        RoomName = room.Name ?? "",
                        Capacity = room.Capacity,
                        Floor = room.Floor,
                        Status = room.Status.Name ?? "",
                        Image = Convert.ToBase64String(room.ImageMaps.FirstOrDefault().Image.ImageData ?? new Byte[1]),
                        Bookings = room.Bookings
                            .Where(b => string.IsNullOrWhiteSpace(request.UserId) || b.UserId == Int32.Parse(request.UserId))
                            .Where(booking =>
                                request.Date.Date == booking.StartTime.Date
                                && booking.IsDelete == false)
                            .Select(booking => new BookingDetails
                            {
                                BookingId = booking.Id,
                                Creator = booking.User.Username,
                                BookingName = booking.Name,
                                StartTime = booking.StartTime,
                                EndTime = booking.EndTime,
                                AttendeesList = booking.AttendeesList
                            }).ToList()
                    }).ToList()
                })
               .ToListAsync();

            return result;
        }

        public async Task<BookingDetailResponse> GetBookingsDetail(int bookingId)
        {
            Booking booking = await _dbContext.Bookings
               .Where(x => x.Id == bookingId)
               .Include(x => x.User)
               .Include(x => x.Room)
               .ThenInclude(x => x.Status)
               .FirstOrDefaultAsync();

            if (booking == null)
            {
                _logger.LogError("Cannot find the booking by id:{id}", bookingId);
                throw new ArgumentException("Cannot find the booking");
            }

            return _mapper.Map<BookingDetailResponse>(booking);
        }

        public async Task<List<AvaiableRoomResponse>> GetAvailableRooms(DateTime beginTime, DateTime endTime, string location, int capacity, int roomTypeId)
        {
            var bookedRooms = await _dbContext.Bookings
                        .Where(b => (b.StartTime < endTime && b.EndTime > beginTime) || (b.StartTime >= beginTime && b.StartTime < endTime))
                        .Where(x => x.IsDelete == false)
                        .Select(b => b.RoomId)
                        .Distinct()
                        .ToListAsync();

            var avaiableRooms = await _dbContext.Rooms
                        .Where(r => r.IsDelete == false
                        && r.Status.Name != "room in maintain"
                        && !bookedRooms.Contains(r.Id)
                        && (string.IsNullOrWhiteSpace(location) || r.Location == location)
                        && r.Capacity >= capacity
                        && r.RoomTypeId == roomTypeId
                        )
                    .Include(x => x.RoomType)
                    .Include(x => x.Status)
                    .Include(x => x.ImageMaps)
                    .ThenInclude(x => x.Image)
                    .ToListAsync();

            var result = _mapper.Map<List<Room>, List<AvaiableRoomResponse>>(avaiableRooms);

            return result;
        }
    }
}