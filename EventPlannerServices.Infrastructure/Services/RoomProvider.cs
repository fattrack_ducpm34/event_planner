﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using Dapper;

namespace EventPlannerServices.Infrastructure.Services
{
    public class RoomProvider : IRoomProvider
    {
        private readonly BookMeDbContext _dbContext;

        public RoomProvider(BookMeDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Room> GetAllRooms()
        {
            string sql = "SELECT * FROM Rooms";
            return _dbContext.Database.GetDbConnection().Query<Room>(sql);
        }

        public Room GetRoomById(int roomId)
        {
            string sql = "SELECT * FROM Rooms WHERE Id = @Id";
            return _dbContext.Database.GetDbConnection().QueryFirstOrDefault<Room>(sql, new { Id = roomId });
        }

        public IEnumerable<Room> GetRoomsByStatus(int statusId)
        {
            string sql = "SELECT * FROM Rooms WHERE StatusID = @StatusID";
            return _dbContext.Database.GetDbConnection().Query<Room>(sql, new { StatusID = statusId });
        }

        public void AddRoom(Room room)
        {
            string sql = "INSERT INTO Rooms (Capacity, Location, Floor, Name, Description, RoomTypeID, StatusID, IsDelete, CreatedDate, LastModifiedDate) VALUES (@Capacity, @Location, @Floor, @Name, @Description, @RoomTypeID, @StatusID, @IsDelete, @CreatedDate, @LastModifiedDate)";
            _dbContext.Database.GetDbConnection().Execute(sql, room);
        }

        public void UpdateRoom(Room room)
        {
            string sql = "UPDATE Rooms SET Capacity = @Capacity, Location = @Location, Floor = @Floor, Name = @Name, Description = @Description, RoomTypeID = @RoomTypeID, StatusID = @StatusID, IsDelete = @IsDelete, CreatedDate = @CreatedDate, LastModifiedDate = @LastModifiedDate WHERE Id = @Id";
            _dbContext.Database.GetDbConnection().Execute(sql, room);
        }

        public void DeleteRoom(int roomId)
        {
            string sql = "DELETE FROM Rooms WHERE Id = @Id";
            _dbContext.Database.GetDbConnection().Execute(sql, new { Id = roomId });
        }

        public IEnumerable<Room> GetRoomsMatchConditions(string? roomName, string? location, int? roomFloor, int? roomStatus, int? roomType, int? capacity)
        {
            var parameters = new DynamicParameters();
            bool hasConditions = false;

            string sql = "SELECT * FROM Rooms";

            if (!string.IsNullOrEmpty(roomName))
            {
                sql += " WHERE Name LIKE '%' + @RoomName + '%'";
                parameters.Add("@RoomName", roomName);
                hasConditions = true;
            }

            if (!string.IsNullOrEmpty(location))
            {
                sql = AddCondition(sql, hasConditions, "Location = @Location");
                parameters.Add("@Location", location);
                hasConditions = true;
            }

            if (roomFloor.HasValue)
            {
                sql = AddCondition(sql, hasConditions, "Floor = @Floor");
                parameters.Add("@Floor", roomFloor);
                hasConditions = true;
            }

            if (roomStatus.HasValue)
            {
                sql = AddCondition(sql, hasConditions, "StatusID = @RoomStatus");
                parameters.Add("@RoomStatus", roomStatus);
                hasConditions = true;
            }

            if (roomType.HasValue)
            {
                sql = AddCondition(sql, hasConditions, "RoomTypeID = @RoomType");
                parameters.Add("@RoomType", roomType);
                hasConditions = true;
            }

            if (capacity.HasValue)
            {
                sql = AddCondition(sql, hasConditions, "Capacity >= @Capacity");
                parameters.Add("@Capacity", capacity);
                hasConditions = true;
            }

            return _dbContext.Database.GetDbConnection().Query<Room>(sql, parameters);
        }

        public async Task<List<string>> GetLocations()
        {
            var result = await _dbContext.Rooms.Select(r => r.Location).Distinct().ToListAsync();
            return result;
        }

        private string AddCondition(string sql, bool hasConditions, string condition)
        {
            sql += hasConditions ? " AND" : " WHERE";
            sql += " " + condition;
            return sql;
        }
    }
}
