﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;

namespace EventPlannerServices.Infrastructure.Services
{
    public class ImageProvider : IImageProvider
    {
        private readonly BookMeDbContext _dbContext;

        public ImageProvider(BookMeDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddImage(Image image)
        {
            image.CreatedDate = DateTime.UtcNow;
            image.LastModifiedDate = DateTime.UtcNow;

            await _dbContext.AddAsync(image);

            await _dbContext.SaveChangesAsync();
        }

        public Task DeleteImage(string imageId)
        {
            throw new NotImplementedException();
        }

        public async Task<Image> LoadImage(int imageId)
        {
            return await _dbContext.Images.Where(x => x.Id == imageId).FirstAsync();
        }
    }
}