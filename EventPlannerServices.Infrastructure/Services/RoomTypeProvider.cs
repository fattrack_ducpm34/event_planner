﻿using Dapper;
using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;

namespace EventPlannerServices.Infrastructure.Services
{
    public class RoomTypeProvider : IRoomTypeProvider
    {
        private readonly BookMeDbContext _dbContext;

        public RoomTypeProvider(BookMeDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<RoomType> GetAllRoomTypes()
        {
            string sql = "SELECT * FROM RoomTypes";
            return _dbContext.Database.GetDbConnection().Query<RoomType>(sql);
        }
    }
}