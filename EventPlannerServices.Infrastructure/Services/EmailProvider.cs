﻿using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Core.Request;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RestSharp;
using System.Text;
using System.Text.Json;

namespace EventPlannerServices.Infrastructure.Services
{
    public class EmailProvider : IEmailProvider
    {
        private readonly ILogger<EmailProvider> _logger;
        private readonly IConfiguration _configuration;
        private readonly IConnection _connection;
        private readonly IModel _channel;

        public EmailProvider(ILogger<EmailProvider> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;

            var factory = new ConnectionFactory()
            {
                HostName = _configuration.GetSection("Host").Value ?? string.Empty,
                Port = int.Parse(_configuration.GetSection("Port").Value ?? "5672")
            };
            try
            {
                _connection = factory.CreateConnection();
                _channel = _connection.CreateModel();
                _channel.ExchangeDeclare("SendMailExchange", ExchangeType.Direct);
                _channel.QueueDeclare("SendMail", false, false, false, null);
                _channel.QueueBind("SendMail", "SendMailExchange", "SendMailKey", null);

                Console.WriteLine("Connection has been created");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Could not connect to the rabbitmq: {ex.Message}");
            }
        }

        public void PublishNewSendMailRequest(List<SendMailRequest> sendMailRequests)
        {
            var message = JsonSerializer.Serialize(sendMailRequests);

            if (_connection is not null)
            {
                if (_connection.IsOpen)
                {
                    Console.WriteLine("RabbitMQ Connection Is Open");
                    var result = SendMessage(message);
                    if (result is true)
                    {
                        Console.WriteLine($"{message} has been sent");
                    }
                }
            }
        }

        public async Task SendMail(SendMailRequest sendMailRequest)
        {
            try
            {
                var emailServiceUrl = _configuration.GetSection("EmailServiceEndpoint").Value ?? string.Empty;

                if (string.IsNullOrWhiteSpace(emailServiceUrl))
                {
                    _logger.LogError("Cannot find the email service endpoint");
                    throw new ArgumentException();
                }

                var restClient = new RestClient(new Uri(emailServiceUrl));

                RestRequest restRequest = new RestRequest("", Method.Post).AddBody(sendMailRequest);

                var response = await restClient.ExecutePostAsync(restRequest);

                if (!response.IsSuccessful)
                {
                    _logger.LogError("Something wrong happen when sent the email to: {recipient}", sendMailRequest.To);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool SendMessage(string message)
        {
            try
            {
                var body = Encoding.UTF8.GetBytes(message);
                _channel.BasicPublish("SendMailExchange", "SendMailKey", null, body);

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}