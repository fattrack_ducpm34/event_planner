﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Helper;
using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;

namespace EventPlannerServices.Infrastructure.Services
{
    public class UserProvider : IUserProvider
    {
        private readonly BookMeDbContext _dbContext;

        public UserProvider(BookMeDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddUser(User user)
        {
            try
            {
                var passMessage = PasswordHelper.CheckPasswordStrength(user.Password);
                if (!string.IsNullOrEmpty(passMessage))
                {
                    throw new ArgumentException(passMessage);
                }

                bool isAnyUserHasTheSameEmail = await _dbContext.Users.AnyAsync(x => x.Email == user.Email);
                if (isAnyUserHasTheSameEmail)
                {
                    throw new ArgumentException("Email already exist");
                }

                //Just as a simple check because does not have specific requirements in the request
                bool isAnyUserHasTheSameUserName = await _dbContext.Users.AnyAsync(x => x.Username == user.Username);
                if (isAnyUserHasTheSameUserName)
                {
                    throw new ArgumentException("Username already exist ");
                }

                if (!EmailValidator.IsValidEmail(user.Email))
                {
                    throw new ArgumentException("Invalid email address");
                }

                user.Password = PasswordHelper.HashPassword(user.Password);
                user.Token = "";
                //user.RefreshTokenExpiryTime = DateTime.UtcNow;
                //user.RefreshToken = "";
                //user.Fullname = "";

                await _dbContext.AddAsync(user);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new SystemException($"Something wrong happen. Detail: {e.Message}");
            }
        }

        public async Task DeleteUser(int id)
        {
            try
            {
                var userToDelete = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == id);

                if (userToDelete == null)
                {
                    throw new ArgumentException("User does not exist");
                }

                _dbContext.Remove(userToDelete);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new SystemException($"Something wrong happened with the database. Detail: {e.Message}");
            }
        }

        public async Task<User> GetUserById(int id)
        {
            return await _dbContext.Users.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<User> GetUserByUserName(string userName)
        {
            try
            {
                return await _dbContext.Users.FirstOrDefaultAsync(x => x.Username == userName);
            }
            catch (Exception e)
            {
                throw new SystemException($"Something wrong happened with the database. Detail: {e.Message}");
            }
        }

        public async Task<User> GetUserLogin(string userName, string password)
        {
            var loginUser = await _dbContext.Users.FirstOrDefaultAsync(x => x.Username == userName);

            if (loginUser == null)
            {
                throw new ArgumentException("User not found");
            }

            if (!PasswordHelper.VerifyPassword(password, loginUser.Password))
            {
                throw new ArgumentException("Password is incorrect");
            }

            return loginUser;
        }

        public async Task UpdateUser(User user)
        {
            try
            {
                User userToUpdate = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == user.Id);

                if (userToUpdate == null)
                {
                    throw new ArgumentException("User does not exist");
                }

                userToUpdate.Token = user.Token;
                userToUpdate.RefreshToken = user.RefreshToken;
                userToUpdate.RefreshTokenExpiryTime = user.RefreshTokenExpiryTime;

                _dbContext.Update(userToUpdate);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new SystemException($"Something wrong happen with database. Detail: {e.Message}");
            }
        }
    }
}