﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit;
using MimeKit.Text;
using NotificationServices.Core.Interfaces.SPI;
using NotificationServices.Core.Request;

namespace NotificationServices.Infrastructure.Services
{
    public class EmailServiceProvider : IEmailServiceProvider
    {
        private readonly IConfiguration _config;

        public EmailServiceProvider(IConfiguration config)
        {
            _config = config;
        }

        public async Task SendEmailAsync(SendMailRequest request)
        {
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(_config.GetSection("EmailUsername").Value));
            email.To.Add(MailboxAddress.Parse(request.To));
            email.Subject = request.Subject;
            email.Body = new TextPart(TextFormat.Html) { Text = request.Body };

            using (var smtp = new SmtpClient())
            {
                await smtp.ConnectAsync(_config.GetSection("EmailHost").Value, 587, SecureSocketOptions.StartTls);
                await smtp.AuthenticateAsync(_config.GetSection("EmailUsername").Value, _config.GetSection("EmailPassword").Value);
                await smtp.SendAsync(email);
                await smtp.DisconnectAsync(true);
            }
        }

        public async Task SendEmailToRecipients(List<SendMailRequest> requests)
        {
            using (var smtp = new SmtpClient())
            {
                await smtp.ConnectAsync(_config.GetSection("EmailHost").Value, 587, SecureSocketOptions.StartTls);
                await smtp.AuthenticateAsync(_config.GetSection("EmailUsername").Value, _config.GetSection("EmailPassword").Value);

                foreach (var request in requests)
                {
                    var email = new MimeMessage();
                    email.From.Add(MailboxAddress.Parse(_config.GetSection("EmailUsername").Value));
                    email.To.Add(MailboxAddress.Parse(request.To));
                    email.Subject = request.Subject;
                    email.Body = new TextPart(TextFormat.Html) { Text = request.Body };

                    await smtp.SendAsync(email);
                }

                await smtp.DisconnectAsync(true);
            }
        }

    }
}