﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NotificationServices.Core.Interfaces.SPI;
using NotificationServices.Core.Request;

namespace NotificationServices.Infrastructure.Services
{
    public class SendMailEventProcessor : ISendMailEventProcessor
    {
        private readonly ILogger<SendMailEventProcessor> _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public SendMailEventProcessor(ILogger<SendMailEventProcessor> logger, IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public async Task ProcessSendMailEvent(string message)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var emailService = scope.ServiceProvider.GetRequiredService<IEmailServiceProvider>();

                var result = DeserialzeMessage(message);

                if (result is not null)
                {
                    await emailService.SendEmailToRecipients(result);
                    _logger.LogInformation("Email have been sent to {count} recipient(s)", result.Count);
                }
            }
        }

        private List<SendMailRequest> DeserialzeMessage(string message)
        {
            var desMes = JsonConvert.DeserializeObject<List<SendMailRequest>>(message);
            return desMes;
        }
    }
}