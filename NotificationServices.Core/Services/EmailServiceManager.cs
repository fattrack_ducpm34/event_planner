﻿using NotificationServices.Core.Interfaces.API;
using NotificationServices.Core.Interfaces.SPI;
using NotificationServices.Core.Request;

namespace NotificationServices.Core.Services
{
    public class EmailServiceManager : IEmaiServiceManager
    {
        private readonly IEmailServiceProvider _emailServiceProvider;

        public EmailServiceManager(IEmailServiceProvider emailServiceProvider)
        {
            _emailServiceProvider = emailServiceProvider;
        }

        public async Task SendEmailAsync(SendMailRequest request)
        {
            await _emailServiceProvider.SendEmailAsync(request);
        }

        public async Task SendEmailToRecipients(List<SendMailRequest> request)
        {
            await _emailServiceProvider.SendEmailToRecipients(request);
        }
    }
}