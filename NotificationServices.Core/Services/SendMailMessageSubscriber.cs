﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NotificationServices.Core.Interfaces.SPI;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
 
namespace NotificationServices.Core.Services
{
    public class SendMailMessageSubscriber : BackgroundService
    {
        private readonly ILogger<SendMailMessageSubscriber> _logger;
        private readonly IConfiguration _configuration;
        private readonly ISendMailEventProcessor _sendMailEventProcessor;
        private IConnection _connection;
        private IModel _channel;
        private string _queueName;

        public SendMailMessageSubscriber(ILogger<SendMailMessageSubscriber> logger, IConfiguration configuration, ISendMailEventProcessor sendMailEventProcessor)
        {
            _logger = logger;
            _configuration = configuration;
            _sendMailEventProcessor = sendMailEventProcessor;
            InitializeRabbitMQConfiguration();
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (ModuleHandle, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                _sendMailEventProcessor.ProcessSendMailEvent(message);
                Console.WriteLine($"Service Received new message: {message}");
            };

            // Check Queuename is null or not
            // if it is not null listen to the queuename
            if (_queueName is not null)
            {
                _channel.BasicConsume(_queueName, true, consumer);
            }
            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            if (_channel.IsOpen)
            {
                _channel.Close();
                _connection.Close();
            }
            base.Dispose();
        }

        private void InitializeRabbitMQConfiguration()
        {
            var factory = new ConnectionFactory()
            {
                HostName = _configuration.GetSection("Host").Value ?? string.Empty,
                Port = int.Parse(_configuration.GetSection("Port").Value ?? "5672")
            };
            try
            {
                _connection = factory.CreateConnection();
                _channel = _connection.CreateModel();
                _channel.ExchangeDeclare("SendMailExchange", ExchangeType.Direct);

                _channel.QueueDeclare("SendMail",
                     durable: false,
                     exclusive: false,
                     autoDelete: false,
                     arguments: null);

                _queueName = "SendMail";
                _channel.QueueBind(_queueName, "SendMailExchange", "PostKey");

                _logger.LogInformation("Connection has been created");
            }
            catch (Exception ex)
            {
                _logger.LogError("Could not connect to the rabbitmq, detail:{message}", ex.Message);
            }
        }
    }
}