﻿using NotificationServices.Core.Request;

namespace NotificationServices.Core.Interfaces.API
{
    public interface IEmaiServiceManager
    {
        Task SendEmailAsync(SendMailRequest request);

        Task SendEmailToRecipients(List<SendMailRequest> request);
    }
}