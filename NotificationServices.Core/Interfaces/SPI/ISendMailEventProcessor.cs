﻿namespace NotificationServices.Core.Interfaces.SPI
{
    public interface ISendMailEventProcessor
    {
        Task ProcessSendMailEvent(string message);
    }
}