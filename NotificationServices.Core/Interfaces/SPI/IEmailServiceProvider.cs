﻿using NotificationServices.Core.Request;

namespace NotificationServices.Core.Interfaces.SPI
{
    public interface IEmailServiceProvider
    {
        Task SendEmailAsync(SendMailRequest request);

        Task SendEmailToRecipients(List<SendMailRequest> request);
    }
}