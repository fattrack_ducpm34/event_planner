﻿using AutoMapper;
using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Responses;

namespace EventPlannerServices.Core.Mapper
{
    public class MappingResponseProfile : Profile
    {
        public MappingResponseProfile()
        {
            CreateMap<Room, AvaiableRoomResponse>()
            .ForMember(dest => dest.RoomType, opt => opt.MapFrom(src => src.RoomType.Name))
            .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.Name))
            .ForMember(dest => dest.ImageData, opt => opt.MapFrom(src => src.ImageMaps.Any() ? src.ImageMaps.First().Image.ImageData : null))
            .ReverseMap();

            CreateMap<Booking, BookingDetailResponse>()
            .ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.Room.Location ?? ""))
            .ForMember(dest => dest.RoomName, opt => opt.MapFrom(src => src.Room.Name ?? ""))
            .ForMember(dest => dest.Capacity, opt => opt.MapFrom(src => src.Room.Capacity))
            .ForMember(dest => dest.Floor, opt => opt.MapFrom(src => src.Room.Floor))
            .ForMember(dest => dest.RoomStatus, opt => opt.MapFrom(src => src.Room.Status.Name ?? ""))
            .ForMember(dest => dest.BookingId, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.BookingName, opt => opt.MapFrom(src => src.Name))
            .ForMember(dest => dest.Creator, opt => opt.MapFrom(src => src.User.Email ?? ""))
            .ReverseMap();
        }
    }
}