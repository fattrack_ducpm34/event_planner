﻿namespace EventPlannerServices.Core.Interfaces.SPI
{
    public interface IUserInfoProvider
    {
        public Task ChangePassword(int id, string oldPassword, string newPassword);

        // Todo ducpm34
        public Task UpdateUserFullName(int id, string fullName);
    }
}