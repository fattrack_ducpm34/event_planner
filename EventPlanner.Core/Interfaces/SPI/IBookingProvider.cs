﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Request;
using EventPlannerServices.Core.Responses;

namespace EventPlannerServices.Core.Interfaces.SPI
{
    public interface IBookingProvider
    {
        public string AddBooking(int userId, int roomId, DateTime startTime, DateTime endTime, string bookingName, string bookingDescription, string attendeesList);

        public Task<Booking> UpdateReservation(int bookingId, int userId, DateTime startTime, DateTime endTime, string bookingName, string bookingDescription, string attendeesList);

        public Task<List<BookingInfoResponse>> GetBookingsInfo(GetBookingInfoRequest request);

        public Task<BookingDetailResponse> GetBookingsDetail(int BookingId);

        public Task<Booking> CancelReservation(int bookingId, int userId);

        public Task<List<AvaiableRoomResponse>> GetAvailableRooms(DateTime beginTime, DateTime endTime, string location, int capacity, int roomTypeId);
    }
}