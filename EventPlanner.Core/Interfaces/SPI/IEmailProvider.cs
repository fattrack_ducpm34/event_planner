﻿using EventPlannerServices.Core.Request;

namespace EventPlannerServices.Core.Interfaces.SPI
{
    public interface IEmailProvider
    {
        Task SendMail(SendMailRequest request);

        void PublishNewSendMailRequest(List<SendMailRequest> sendMailRequests);
    }
}