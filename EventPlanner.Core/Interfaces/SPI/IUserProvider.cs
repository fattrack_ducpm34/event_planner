﻿using EventPlannerServices.Core.Entities;

namespace EventPlannerServices.Core.Interfaces.SPI
{
    public interface IUserProvider
    {
        public Task AddUser(User user);

        public Task UpdateUser(User user);

        public Task DeleteUser(int id);

        public Task<User> GetUserById(int id);

        public Task<User> GetUserLogin(string userName, string password);

        public Task<User> GetUserByUserName(string userName);
    }
}