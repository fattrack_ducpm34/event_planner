﻿using EventPlannerServices.Core.Entities;

namespace EventPlannerServices.Core.Interfaces.SPI
{
    public interface IUserRoleProvider
    {
        public Task<List<UserRole>> GetUserRolesByUserId(int userId);

        public Task AddRoleForUser(int userId, int roleId);

        public Task RemoveRoleOfUser(int userId, int roleId);
    }
}