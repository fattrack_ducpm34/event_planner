﻿using EventPlannerServices.Core.Entities;

namespace EventPlannerServices.Core.Interfaces.SPI
{
    public interface IImageProvider
    {
        Task AddImage(Image image);

        Task<Image> LoadImage(int imageId);

        Task DeleteImage(string imageId);
    }
}