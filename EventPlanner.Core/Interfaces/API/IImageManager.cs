﻿
using EventPlannerServices.Core.Entities;

namespace EventPlannerServices.Core.Interfaces.API
{
    public interface IImageManager
    {
        Task AddImage(Image image);

        Task<Image> LoadImage(int imageId);

        Task DeleteImage(string imageId);
    }
}