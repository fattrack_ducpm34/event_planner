﻿using EventPlannerServices.Core.Entities;

namespace EventPlannerServices.Core.Interfaces.API
{
    public interface IUserManager
    {
        public Task AddUser(User user);

        public Task UpdateUser(User user);

        public Task DeleteUser(int id);

        public Task<User> GetUserLogin(string userName, string password);

        public Task<User> GetUserByUserName(string userName);
    }
}