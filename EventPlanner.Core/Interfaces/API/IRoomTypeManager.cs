﻿using EventPlannerServices.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventPlannerServices.Core.Interfaces.API
{
    public interface IRoomTypeManager
    {
        public IEnumerable<RoomType> GetAllRoomTypes();
    }
}