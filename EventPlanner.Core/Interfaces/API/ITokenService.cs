﻿using EventPlannerServices.Core.Entities;
using System.Security.Claims;

namespace EventPlannerServices.Core.Interfaces.API
{
    public interface ITokenService
    {
        Task<string> CreateJwt(User user);

        public string CreateRefreshToken(User user);

        public Task<ClaimsPrincipal> GetPrincipleFromExpiredToken(string token);
    }
}