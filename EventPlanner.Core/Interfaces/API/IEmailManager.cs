﻿using EventPlannerServices.Core.Request;

namespace EventPlannerServices.Core.Interfaces.API
{
    public interface IEmailManager
    {
        Task SendMail(SendMailRequest request);

        void PublishNewSendMailRequest(List<SendMailRequest> sendMailRequests);
    }
}