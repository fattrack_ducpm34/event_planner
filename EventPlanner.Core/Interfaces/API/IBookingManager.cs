﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Request;
using EventPlannerServices.Core.Responses;

namespace EventPlannerServices.Core.Interfaces.API
{
    public interface IBookingManager
    {
        public Task AddBooking(int userId, int roomId, DateTime startTime, DateTime endTime, string bookingName, string bookingDescription, string attendeesList);

        public Task<List<BookingInfoResponse>> GetBookingsInfo(GetBookingInfoRequest request);

        public Task CancelReservation(int bookingId, int userId);

        public Task UpdateReservation(int bookingId, int userId, DateTime startTime, DateTime endTime, string bookingName, string bookingDescription, string attendeesList);

        public Task<BookingDetailResponse> GetBookingInfoDetail(int bookingId);

        public Task<List<AvaiableRoomResponse>> GetAvailableRooms(DateTime beginTime, DateTime endTime, string location, int capacity, int roomTypeId);
    }
}