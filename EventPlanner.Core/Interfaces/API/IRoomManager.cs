﻿using EventPlannerServices.Core.Entities;

namespace EventPlannerServices.Core.Interfaces.API
{
    public interface IRoomManager
    {
        public IEnumerable<Room> GetAllRooms();

        public Room GetRoomById(int roomId);

        public IEnumerable<Room> GetRoomsByStatus(int statusId);

        public void AddRoom(Room room);

        public void UpdateRoom(Room room);

        public void DeleteRoom(int roomId);

        public IEnumerable<Room> GetRoomsMatchConditions(string? roomName, string? location, int? roomFloor = null, int? roomStatus = null, int? roomType = null, int? capacity = null);

        Task<List<string>> GetLocations();
    }
}