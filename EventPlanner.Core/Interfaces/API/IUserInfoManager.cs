﻿namespace EventPlannerServices.Core.Interfaces.API
{
    public interface IUserInfoManager
    {
        public Task UpdateUserName(int id, string fullName);

        public Task ChangePassword(int id, string oldPassword, string newPassword);
    }
}