﻿using EventPlannerServices.Core.Entities;

namespace EventPlannerServices.Core.Interfaces.API
{
    public interface IRoleManager
    {
        public Task AddNewRole(Role role);
    }
}