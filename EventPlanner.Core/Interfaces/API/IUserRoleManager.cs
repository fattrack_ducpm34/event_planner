﻿namespace EventPlannerServices.Core.Interfaces.API
{
    public interface IUserRoleManager
    {
        public Task AddRoleForUser(int userId, int roleId);

        public Task RemoveRoleOfUser(int userId, int roleId);
    }
}