﻿namespace EventPlannerServices.Core.Responses
{
    public class BookingInfoResponse
    {
        public string Location { get; set; }
        public List<RoomInfo> Rooms { get; set; }
    }

    public class RoomInfo
    {
        public int Id { get; set; }
        public string RoomName { get; set; }
        public int? Capacity { get; set; }
        public int? Floor { get; set; }
        public string Status { get; set; }  
        public string Image { get; set; }
        public List<BookingDetails> Bookings { get; set; }
    }

    public class BookingDetails
    {
        public int BookingId { get; set; }
        public string? BookingName { get; set; } = string.Empty;
        public string? Creator { get; set; } = string.Empty;
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string? AttendeesList { get; set; } = string.Empty;
    }
}