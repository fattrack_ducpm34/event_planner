﻿namespace EventPlannerServices.Core.Responses
{
    public class AvaiableRoomResponse
    {
        public int Id { get; set; }

        public int? Capacity { get; set; }

        public string? Location { get; set; }

        public int? Floor { get; set; }

        public string? Name { get; set; }

        public string? Description { get; set; }

        public string Status { get; set; }

        public string RoomType { get; set; }

        public Byte[] ImageData { get; set; }
    }
}