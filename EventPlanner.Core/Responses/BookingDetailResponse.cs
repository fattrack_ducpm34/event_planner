﻿namespace EventPlannerServices.Core.Responses
{
    public class BookingDetailResponse
    {
        public string Location { get; set; }
        public int UserId { get; set; }
        public int RoomId { get; set; }

        public string RoomName { get; set; }

        public int Capacity { get; set; }
        public int Floor { get; set; }

        public string RoomStatus { get; set; }

        public int BookingId { get; set; }

        public string BookingName { get; set; }

        public string Creator { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public string AttendeesList { get; set; }
    }
}