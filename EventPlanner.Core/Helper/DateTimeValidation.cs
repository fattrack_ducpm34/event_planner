﻿namespace EventPlannerServices.Core.Helper
{
    public static class DateTimeValidation
    {
        public static bool ValidateDateTime(DateTime startTime, DateTime endTime)
        {
            // Check if StartTime and EndTime are on the same date
            if (startTime.Date != endTime.Date)
            {
                return false;
            }

            // Check if StartTime is before EndTime
            if (startTime >= endTime)
            {
                return false;
            }

            // Check if StartTime and EndTime are at least 30 minutes apart
            //if ((endTime - startTime).TotalMinutes < 30)
            //{
            //    return false;
            //}

            // Check if the time difference between startTime and endTime is a multiple of 30 minutes
            if ((endTime - startTime).TotalMinutes % 30 != 0)
            {
                return false;
            }

            // Check if StartTime and EndTime fall within the time frame of 7:00 AM to 6:30 PM
            TimeSpan startTimeOfDay = startTime.TimeOfDay;
            TimeSpan endTimeOfDay = endTime.TimeOfDay;

            TimeSpan validStartTime = new TimeSpan(7, 0, 0); // 7:00 AM
            TimeSpan validEndTime = new TimeSpan(19, 0, 0); // 7:00 PM

            if (startTimeOfDay < validStartTime || endTimeOfDay > validEndTime)
            {
                return false;
            }

            return true;
        }
    }
}