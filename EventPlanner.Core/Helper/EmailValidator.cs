﻿using System.Text.RegularExpressions;

namespace EventPlannerServices.Core.Helper
{
    /// <summary>
    /// Provides functionality to validate email addresses.
    /// </summary>
    public class EmailValidator
    {
        // Regular expression pattern for validating email addresses
        private static readonly Regex EmailRegex = new Regex(@"^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$");

        /// <summary>
        /// Validates whether the given email address is valid.
        /// </summary>
        /// <param name="email">The email address to be validated.</param>
        /// <returns>True if the email address is valid, otherwise false.</returns>
        public static bool IsValidEmail(string email)
        {
            // Check if the email is null, empty, or contains only whitespace
            if (string.IsNullOrWhiteSpace(email))
                return false;

            // Trim leading and trailing whitespaces from the email
            email = email.Trim();

            // Check if the length of the email exceeds the maximum allowed length (256 characters)
            if (email.Length > 256)
                return false;

            // Use regular expression to validate the email format
            return EmailRegex.IsMatch(email);
        }
    }
}
