﻿namespace EventPlannerServices.Core.Entities;

public partial class Room : IEntity
{
    public int Id { get; set; }

    public int Capacity { get; set; }

    public string Location { get; set; }

    public int Floor { get; set; }

    public string? Name { get; set; }

    public string? Description { get; set; }

    public int? RoomTypeId { get; set; }

    public int? StatusId { get; set; }

    public bool IsDelete { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? LastModifiedDate { get; set; }

    public virtual ICollection<Booking> Bookings { get; set; } = new List<Booking>();

    public virtual ICollection<ImageMap> ImageMaps { get; set; } = new List<ImageMap>();

    public virtual RoomType? RoomType { get; set; }

    public virtual RoomStatus Status { get; set; }
}