﻿namespace EventPlannerServices.Core.Entities;

public partial class Role : IEntity
{
    public int Id { get; set; }

    public string? Code { get; set; }

    public string? Description { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? LastModifiedDate { get; set; }

    public virtual ICollection<UserRole> UserRoles { get; set; } = new List<UserRole>();
}