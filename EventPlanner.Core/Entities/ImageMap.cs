﻿namespace EventPlannerServices.Core.Entities;

public partial class ImageMap
{
    public int Id { get; set; }

    public int? ImageId { get; set; }

    public int? RoomId { get; set; }

    public virtual Image? Image { get; set; }

    public virtual Room? Room { get; set; }
}