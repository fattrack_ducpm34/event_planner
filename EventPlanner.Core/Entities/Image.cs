﻿namespace EventPlannerServices.Core.Entities;

public partial class Image : IEntity
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public byte[]? ImageData { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? LastModifiedDate { get; set; }

    public virtual ICollection<ImageMap> ImageMaps { get; set; } = new List<ImageMap>();
}