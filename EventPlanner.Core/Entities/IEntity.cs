﻿namespace EventPlannerServices.Core.Entities
{
    public interface IEntity
    {
        public int Id { get; set; }
        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }
    }
}