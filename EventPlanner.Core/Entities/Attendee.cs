﻿namespace EventPlannerServices.Core.Entities;

public partial class Attendee : IEntity
{
    public int Id { get; set; }

    public int? BookingId { get; set; }

    public string? Email { get; set; }

    public int? AttendeeStatusId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? LastModifiedDate { get; set; }

    public virtual AttendeeStatus? AttendeeStatus { get; set; }

    public virtual Booking? Booking { get; set; }
}