﻿namespace EventPlannerServices.Core.Entities;

public partial class RoomType : IEntity
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? Description { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? LastModifiedDate { get; set; }

    public virtual ICollection<Room> Rooms { get; set; } = new List<Room>();
}