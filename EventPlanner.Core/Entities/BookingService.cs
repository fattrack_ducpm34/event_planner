﻿namespace EventPlannerServices.Core.Entities;

public partial class BookingService
{
    public int BookingId { get; set; }

    public int ServiceId { get; set; }

    public bool? IsPaid { get; set; }

    public bool? IsDelete { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? LastModifiedDate { get; set; }

    public virtual Booking Booking { get; set; } = null!;

    public virtual Service Service { get; set; } = null!;
}