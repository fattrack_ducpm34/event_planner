﻿namespace EventPlannerServices.Core.Entities;

public partial class Booking : IEntity
{
    public int Id { get; set; }

    public int UserId { get; set; }

    public int RoomId { get; set; }

    public string? Name { get; set; }

    public string? AttendeesList { get; set; }

    public string? Description { get; set; }

    public DateTime StartTime { get; set; }

    public DateTime EndTime { get; set; }

    public bool? IsDelete { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? LastModifiedDate { get; set; }

    public virtual ICollection<Attendee> Attendees { get; set; } = new List<Attendee>();

    public virtual ICollection<BookingService> BookingServices { get; set; } = new List<BookingService>();

    public virtual Room? Room { get; set; }

    public virtual User? User { get; set; }
}