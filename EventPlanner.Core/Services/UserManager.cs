﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;
using Microsoft.Extensions.Logging;
using System.Data;

namespace EventPlannerServices.Core.Services
{
    public class UserManager : IUserManager
    {
        private readonly ILogger<UserManager> _logger;
        private readonly IGenericRepository<User> _userRepository;
        private readonly IUserProvider _userProvider;
        private readonly IUserRoleManager _userRoleManager;

        public UserManager(ILogger<UserManager> logger, IGenericRepository<User> userRepository, IUserProvider userProvider, IUserRoleManager userRoleManager)
        {
            _logger = logger;
            _userRepository = userRepository;
            _userProvider = userProvider;
            _userRoleManager = userRoleManager;
        }

        public async Task AddUser(User user)
        {
            try
            {
                _logger.LogInformation("Starting to create new user");

                // when the user signs up, he will has the default role
                user.UserRoles = new List<UserRole>()
                {
                    new UserRole
                    {
                        RoleId = 2
                    }
                };

                await _userProvider.AddUser(user);

                _logger.LogInformation("Creating user finished");
            }
            catch (Exception ex)
            {
                _logger.LogError("Something wrong happen. Detail: {message}", ex.Message);
                throw;
            }
        }

        public async Task DeleteUser(int id)
        {
            try
            {
                _logger.LogInformation("Starting to delete user");

                await _userRepository.Delete(id);

                _logger.LogInformation("Deleting user finished");
            }
            catch (Exception ex)
            {
                _logger.LogError("Something wrong happen. Detail: {message}", ex.Message);
                throw;
            }
        }

        public async Task<User> GetUserByUserName(string userName)
        {
            try
            {
                return await _userProvider.GetUserByUserName(userName);
            }
            catch (Exception ex)
            {
                _logger.LogError("Something wrong happen. Detail: {message}", ex.Message);
                throw;
            }
        }

        public async Task<User> GetUserLogin(string userName, string password)
        {
            try
            {
                return await _userProvider.GetUserLogin(userName, password);
            }
            catch (Exception ex)
            {
                _logger.LogError("Something wrong happen. Detail: {message}", ex.Message);
                throw;
            }
        }

        public async Task UpdateUser(User user)
        {
            try
            {
                _logger.LogInformation("Starting to update new user");

                await _userProvider.UpdateUser(user);

                _logger.LogInformation("Update user finished");
            }
            catch (Exception ex)
            {
                _logger.LogError("Something wrong happen. Detail: {message}", ex.Message);
                throw;
            }
        }
    }
}