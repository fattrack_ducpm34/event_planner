﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;

namespace EventPlannerServices.Core.Services
{
    public class ImageManager : IImageManager
    {
        private readonly IImageProvider _imageProvider;

        public ImageManager(IImageProvider imageProvider)
        {
            _imageProvider = imageProvider;
        }

        public async Task AddImage(Image image)
        {
            await _imageProvider.AddImage(image);
        }

        public Task DeleteImage(string imageId)
        {
            throw new NotImplementedException();
        }

        public async Task<Image> LoadImage(int imageId)
        {
            return await _imageProvider.LoadImage(imageId);
        }
    }
}