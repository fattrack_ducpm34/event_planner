﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;
using Microsoft.Extensions.Logging;

namespace EventPlannerServices.Core.Services
{
    public class RoomManager : IRoomManager
    {
        private readonly ILogger<RoomManager> _logger;
        private readonly IRoomProvider _roomProvider;

        public RoomManager(ILogger<RoomManager> logger, IRoomProvider roomProvider)
        {
            _logger = logger;
            _roomProvider = roomProvider;
        }

        public IEnumerable<Room> GetAllRooms()
        {
            try
            {
                _logger.LogInformation("RoomManager.GetAllRooms: Starting to get all rooms");

                var rooms = _roomProvider.GetAllRooms();

                _logger.LogInformation("RoomManager.GetAllRooms: Get all rooms finished");

                return rooms;
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomManager.GetAllRooms: Error getting all rooms. Detail: {message}", ex.Message);
                throw;
            }
        }

        public Room GetRoomById(int roomId)
        {
            try
            {
                _logger.LogInformation("RoomManager.GetRoomById: Starting to get room by ID");

                var room = _roomProvider.GetRoomById(roomId);

                _logger.LogInformation("RoomManager.GetRoomById: Get room by ID finished");

                return room;
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomManager.GetRoomById: Error getting room by ID. Detail: {message}", ex.Message);
                throw;
            }
        }

        public IEnumerable<Room> GetRoomsByStatus(int statusId)
        {
            try
            {
                _logger.LogInformation("RoomManager.GetRoomsByStatus: Starting to get rooms by status");

                var rooms = _roomProvider.GetRoomsByStatus(statusId);

                _logger.LogInformation("RoomManager.GetRoomsByStatus: Get rooms by status finished");

                return rooms;
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomManager.GetRoomsByStatus: Error getting rooms by status. Detail: {message}", ex.Message);
                throw;
            }
        }

        public void AddRoom(Room room)
        {
            try
            {
                _logger.LogInformation("RoomManager.AddRoom: Starting to add new room");

                if (IsValidRoom(room))
                {
                    _roomProvider.AddRoom(room);

                    _logger.LogInformation("RoomManager.AddRoom: Add room finished");
                }
                else
                {
                    throw new ArgumentException("Invalid room information");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomManager.AddRoom: Can not add room. Detail: {message}", ex.Message);
                throw;
            }
        }

        public void UpdateRoom(Room room)
        {
            try
            {
                _logger.LogInformation("RoomManager.UpdateRoom: Starting to update room");

                if (IsValidRoom(room))
                {
                    _roomProvider.UpdateRoom(room);

                    _logger.LogInformation("RoomManager.UpdateRoom: Update room finished");
                }
                else
                {
                    throw new ArgumentException("Invalid room information");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomManager.UpdateRoom: Can not update room. Detail: {message}", ex.Message);
                throw;
            }
        }

        public void DeleteRoom(int roomId)
        {
            try
            {
                _logger.LogInformation("RoomManager.DeleteRoom: Starting to delete room");

                var roomToDelete = _roomProvider.GetRoomById(roomId);
                if (roomToDelete != null && CanDeleteRoom(roomToDelete))
                {
                    _roomProvider.DeleteRoom(roomId);

                    _logger.LogInformation("RoomManager.DeleteRoom: Delete room finished");
                }
                else
                {
                    throw new InvalidOperationException("Cannot delete the room");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomManager.DeleteRoom: Can not delete room. Detail: {message}", ex.Message);
                throw;
            }
        }

        public IEnumerable<Room> GetRoomsMatchConditions(string? roomName, string? location, int? roomFloor, int? roomStatus, int? roomType, int? capacity)
        {
            try
            {
                _logger.LogInformation("RoomManager.GetRoomsMatchConditions: Starting to get rooms with conditions");

                var rooms = _roomProvider.GetRoomsMatchConditions(roomName, location, roomFloor, roomStatus, roomType, capacity);

                _logger.LogInformation("RoomManager.GetRoomsMatchConditions: Get rooms with conditions finished");

                return rooms;
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomManager.GetRoomsMatchConditions: Error getting rooms with conditions. Detail: {message}", ex.Message);
                throw;
            }
        }

        public async Task<List<string>> GetLocations()
        {
            return  await _roomProvider.GetLocations();
        }

        private bool IsValidRoom(Room room)
        {
            return !string.IsNullOrEmpty(room.Name) && room.Capacity > 0;
        }

        private bool CanDeleteRoom(Room room)
        {
            return true;
        }
    }
}
