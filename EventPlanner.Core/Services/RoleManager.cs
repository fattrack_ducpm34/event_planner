﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;
using Microsoft.Extensions.Logging;

namespace EventPlannerServices.Core.Services
{
    public class RoleManager : IRoleManager
    {
        private readonly ILogger<RoleManager> _logger;
        private readonly IGenericRepository<Role> _roleRepository;

        public RoleManager(ILogger<RoleManager> logger, IGenericRepository<Role> roleRepository)
        {
            _logger = logger;
            _roleRepository = roleRepository;
        }
        public async Task AddNewRole(Role role)
        {
            try
            {
                _logger.LogInformation("Starting to create new role");

                await _roleRepository.Create(role);

                _logger.LogInformation("Creating role finished");
            }
            catch (Exception ex)
            {
                _logger.LogError("Can not create the role. Detail: {message}", ex.Message);
                throw;
            }
        }
    }
}