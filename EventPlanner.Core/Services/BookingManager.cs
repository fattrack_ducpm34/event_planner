﻿using EventPlannerServices.Core.Helper;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Core.Request;
using EventPlannerServices.Core.Responses;
using Microsoft.Extensions.Logging;

namespace EventPlannerServices.Core.Services
{
    public class BookingManager : IBookingManager
    {
        private readonly ILogger<BookingManager> _logger;
        private readonly IBookingProvider _bookingProvider;
        private readonly IEmailManager _emailManager;
        private readonly IRoomProvider _roomProvider;
        private readonly IUserProvider _userProvider;

        public BookingManager(
            ILogger<BookingManager> logger,
            IBookingProvider bookingProvider,
            IEmailManager emailManager,
            IRoomProvider roomProvider,
            IUserProvider userProvider)
        {
            _logger = logger;
            _bookingProvider = bookingProvider;
            _emailManager = emailManager;
            _roomProvider = roomProvider;
            _userProvider = userProvider;
        }

        public async Task AddBooking(int userId, int roomId, DateTime startTime, DateTime endTime, string bookingName, string bookingDescription, string attendeesList)
        {
            try
            {
                if (!DateTimeValidation.ValidateDateTime(startTime, endTime))
                {
                    _logger.LogError("Invalid timeframe");
                    throw new ArgumentException("Invalid timeframe");
                }

                _logger.LogInformation("Start add booking");
                var reuslt = _bookingProvider.AddBooking(userId, roomId, startTime, endTime, bookingName, bookingDescription, attendeesList);

                if(!string.IsNullOrWhiteSpace(reuslt))
                {
                    _logger.LogError("Something wrong happend. Details:{error}", reuslt);
                    throw new ArgumentException(reuslt);
                }

                var roomInfo = _roomProvider.GetRoomById(roomId);
                var user = await _userProvider.GetUserById(userId);

                // Send Notification Email to AttendeesList
                string emailAddressList = "";

                if (string.IsNullOrWhiteSpace(attendeesList))
                {
                    attendeesList = user.Email;
                };

                emailAddressList = string.Join(",", attendeesList ?? "", user.Email);

                NotifyUpcomingMeeting("Upcoming Meeting Notification", attendeesList, bookingName, bookingDescription, startTime, endTime, roomInfo.Location ?? "", roomInfo.Floor, roomInfo.Name);

                _logger.LogInformation("Add booking finish");
            }
            catch (Exception ex)
            {
                _logger.LogError("Can not add booking. Detail: {message}", ex.Message);
                throw;
            }
        }

        public async Task CancelReservation(int bookingId, int userId)
        {
            try
            {
                _logger.LogInformation("Starting to cancel the reservation");

                var booking = await _bookingProvider.CancelReservation(bookingId, userId);

                string emailAddressList = string.Join(",", booking.AttendeesList ?? "", booking.User.Email);

                NotifyCanceledMeeting(emailAddressList, booking.Name, booking.Description, booking.StartTime, booking.EndTime, booking.Room.Location, booking.Room.Floor);

                _logger.LogInformation("Finished canceling the reservation");
            }
            catch (Exception ex)
            {
                _logger.LogError("Can not cancel the reservation. Detail: {message}", ex.Message);
                throw;
            }
        }

        public async Task<BookingDetailResponse> GetBookingInfoDetail(int bookingId)
        {
            try
            {
                _logger.LogInformation("Starting to get the booking info details");

                return await _bookingProvider.GetBookingsDetail(bookingId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Cannot get the booking info by Id {id}. Detail: {message}", bookingId, ex.Message);
                throw;
            }
        }

        public async Task<List<BookingInfoResponse>> GetBookingsInfo(GetBookingInfoRequest request)
        {
            try
            {
                _logger.LogInformation("Starting to get the reservation info");

                return await _bookingProvider.GetBookingsInfo(request);
            }
            catch (Exception ex)
            {
                _logger.LogError("Cannot get the reservation info. Detail: {message}", ex.Message);
                throw;
            }
        }

        public async Task UpdateReservation(int bookingId, int userId, DateTime startTime, DateTime endTime, string bookingName, string bookingDescription, string attendeesList)
        {
            try
            {
                if (!DateTimeValidation.ValidateDateTime(startTime, endTime))
                {
                    _logger.LogError("Invalid timeframe");
                    throw new ArgumentException("Invalid timeframe");
                }

                _logger.LogInformation("Starting to update reservation");

                var booking = await _bookingProvider.UpdateReservation(bookingId, userId, startTime, endTime, bookingName, bookingDescription, attendeesList);

                string emailAddressList = string.Join(",", booking.AttendeesList ?? "", booking.User.Email);

                // Send Notification Email to AttendeesList
                NotifyUpcomingMeeting("Updated Meeting Notification", attendeesList, bookingName, bookingDescription, startTime, endTime, booking.Room.Location ?? "", booking.Room.Floor, booking.Room.Name);

                _logger.LogInformation("Finished updating reservation");
            }
            catch (Exception ex)
            {
                _logger.LogError("Cannot update the reservation. Detail: {message}", ex.Message);
                throw;
            }
        }

        public async Task<List<AvaiableRoomResponse>> GetAvailableRooms(DateTime beginTime, DateTime endTime, string location, int capacity, int roomTypeId)
        {
            try
            {
                if (beginTime >= endTime)
                {
                    _logger.LogError("Invalid timeframe");
                    throw new ArgumentException("Invalid timeframe");
                }

                var result = await _bookingProvider.GetAvailableRooms(beginTime, endTime, location, capacity, roomTypeId);

                if (result.Count == 0)
                {
                    _logger.LogWarning("No avaiable room");
                    return new List<AvaiableRoomResponse>();
                }

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("Something wrong happen when search avaiable room. Detail: {detail}", ex.Message);
                throw;
            }
        }

        private void NotifyUpcomingMeeting(string subject, string attendeesList, string bookingName, string bookingDescription, DateTime startTime, DateTime endTime, string location, int? floor, string roomName)
        {
            string[] emailAddresses = attendeesList.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            List<SendMailRequest> mailRequests = emailAddresses
                .Select(email => new SendMailRequest
                {
                    To = email.Trim(),
                    Subject = subject,
                    Body = $"Dear Attendees,\n\nThis is to inform you about an upcoming meeting. Details are as follows:\n\nMeeting Subject: {bookingName}. Note: {bookingDescription}\nDate: {startTime}\nTo: {endTime}\n.Room: {roomName} .Location: {location}. Floor: {floor}.\n\nPlease make sure to attend on time. If you have any questions or concerns, feel free to reach out.\n\nBest Regards,\nThe Organizer"
                }).ToList();

            _emailManager.PublishNewSendMailRequest(mailRequests);

            // Or we can use the APIGateWay instead of RabbitMq
        }

        private void NotifyCanceledMeeting(string attendeesList, string bookingName, string bookingDescription, DateTime startTime, DateTime endTime, string location, int? floor)
        {
            string[] emailAddresses = attendeesList.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            List<SendMailRequest> mailRequests = emailAddresses
                .Select(email => new SendMailRequest
                {
                    To = email.Trim(),
                    Subject = "Canceled Meeting Notification",
                    Body = $"Dear Attendees,\n\nWe regret to inform you that the meeting has been canceled. Details are as follows:\n\nMeeting Subject: {bookingName}. Note: {bookingDescription}\nOriginal Date: {startTime}\nTo: {endTime}\nLocation: {location}. Floor: {floor}.\n\nWe apologize for any inconvenience caused. If you have any questions or concerns, feel free to reach out.\n\nBest Regards,\nThe Organizer"
                }).ToList();

            _emailManager.PublishNewSendMailRequest(mailRequests);

            // Or use the APIGateWay instead of RabbitMq
        }
    }
}