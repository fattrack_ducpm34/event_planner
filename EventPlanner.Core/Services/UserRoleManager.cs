﻿using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;
using Microsoft.Extensions.Logging;

namespace EventPlannerServices.Core.Services
{
    public class UserRoleManager : IUserRoleManager
    {
        private readonly ILogger<UserRoleManager> _logger;
        private readonly IUserRoleProvider _userRoleProvider;

        public UserRoleManager(ILogger<UserRoleManager> logger, IUserRoleProvider userRoleProvider)
        {
            _logger = logger;
            _userRoleProvider = userRoleProvider;
        }

        public async Task AddRoleForUser(int userId, int roleId)
        {
            try
            {
                _logger.LogInformation("Starting to Add role for user");

                await _userRoleProvider.AddRoleForUser(userId, roleId);

                _logger.LogInformation("Adding role for user finished");
            }
            catch (Exception ex)
            {
                _logger.LogError("Something wrong happen. Cannot add role for user. Detail: {message}", ex.Message);
                throw;
            }
        }

        public async Task RemoveRoleOfUser(int userId, int roleId)
        {
            try
            {
                _logger.LogInformation("Starting to Add role for user");

                await _userRoleProvider.RemoveRoleOfUser(userId, roleId);

                _logger.LogInformation("Adding role for user finished");
            }
            catch (Exception ex)
            {
                _logger.LogError("Something wrong happen. Cannot remove role of user. Detail: {message}", ex.Message);
                throw;
            }
        }
    }
}