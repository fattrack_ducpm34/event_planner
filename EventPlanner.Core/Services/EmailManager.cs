﻿using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Core.Request;

namespace EventPlannerServices.Core.Services
{
    public class EmailManager : IEmailManager
    {
        private readonly IEmailProvider _emailProvider;

        public EmailManager(IEmailProvider emailProvider)
        {
            _emailProvider = emailProvider;
        }

        public void PublishNewSendMailRequest(List<SendMailRequest> sendMailRequests)
        {
            _emailProvider.PublishNewSendMailRequest(sendMailRequests);
        }

        public async Task SendMail(SendMailRequest request)
        {
            await _emailProvider.SendMail(request);
        }
    }
}