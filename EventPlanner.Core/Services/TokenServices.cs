﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace EventPlannerServices.Core.Services
{
    public class TokenService : ITokenService
    {
        private readonly ILogger<TokenService> _logger;
        private readonly IUserRoleProvider _userRoleProvider;
        private readonly IConfiguration _config;

        public TokenService(ILogger<TokenService> logger, IUserRoleProvider userRoleProvider, IConfiguration config)
        {
            _logger = logger;
            _userRoleProvider = userRoleProvider;
            _config = config;
        }

        public async Task<string> CreateJwt(User user)
        {
            List<UserRole> userRoles = await _userRoleProvider.GetUserRolesByUserId(user.Id);

            var jwtTokenHandler = new JwtSecurityTokenHandler();

            //TODO DUCPM34: It should move into appsettings.file
            string secretKey = _config.GetSection("SecretKey").Value ?? string.Empty;
            var key = Encoding.ASCII.GetBytes(secretKey);
            var identity = new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, user.Username ?? string.Empty),
                new Claim(ClaimTypes.Email, user.Email ?? string.Empty),
                new Claim("Id", user.Id.ToString())
            });

            if (userRoles.Any())
            {
                foreach (var userRole in userRoles)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, userRole.Role.Code ?? string.Empty));
                }
            }

            var credentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = identity,
                Expires = DateTime.Now.AddMinutes(60),
                SigningCredentials = credentials
            };
            var token = jwtTokenHandler.CreateToken(tokenDescriptor);
            return jwtTokenHandler.WriteToken(token);
        }

        public string CreateRefreshToken(User user)
        {
            var tokenBytes = RandomNumberGenerator.GetBytes(64);
            var refreshToken = Convert.ToBase64String(tokenBytes);

            if (user.RefreshToken != null)
            {
                // Return the existing refresh token if it exists.
                return user.RefreshToken;
            }

            return refreshToken;
        }

        public async Task<ClaimsPrincipal> GetPrincipleFromExpiredToken(string token)
        {
            string secretKey = _config.GetSection("SecretKey").Value ?? string.Empty;
            var key = Encoding.ASCII.GetBytes(secretKey);
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateLifetime = false
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;

            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            {
                _logger.LogError("Invalid Token");
                throw new SecurityTokenException("This is Invalid Token");
            }

            return await Task.FromResult(principal);
        }
    }
}