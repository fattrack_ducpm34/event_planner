﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;
using Microsoft.Extensions.Logging;

namespace EventPlannerServices.Core.Services
{
    public class RoomTypeManager : IRoomTypeManager
    {
        private readonly ILogger<RoomTypeManager> _logger;
        private readonly IRoomTypeProvider _roomTypeProvider;

        public RoomTypeManager(ILogger<RoomTypeManager> logger, IRoomTypeProvider roomTypeProvider)
        {
            _logger = logger;
            _roomTypeProvider = roomTypeProvider;
        }

        public IEnumerable<RoomType> GetAllRoomTypes()
        {
            try
            {
                _logger.LogInformation("RoomTypeManager.GetAllRoomTypes: Starting to get all room types");

                var rooms = _roomTypeProvider.GetAllRoomTypes();

                _logger.LogInformation("RoomTypeManager.GetAllRooms: Get all room types finished");

                return rooms;
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomTypeManager.GetAllRoomTypes: Error getting all room types. Detail: {message}", ex.Message);
                throw;
            }
        }
    }
}