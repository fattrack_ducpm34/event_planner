﻿using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;
using Microsoft.Extensions.Logging;

namespace EventPlannerServices.Core.Services
{
    public class UserInfoManager : IUserInfoManager
    {
        private readonly ILogger<UserInfoManager> _logger;
        private readonly IUserInfoProvider _userInfoProvider;

        public UserInfoManager(ILogger<UserInfoManager> logger, IUserInfoProvider userInfoProvider)
        {
            _logger = logger;
            _userInfoProvider = userInfoProvider;
        }

        public async Task ChangePassword(int id, string oldPassword, string newPassword)
        {
            try
            {
                _logger.LogInformation("Starting to change user password");

                await _userInfoProvider.ChangePassword(id, oldPassword, newPassword);

                _logger.LogInformation("Updating user password finished");
            }
            catch (Exception ex)
            {
                _logger.LogError("Something wrong happen. Cannot update user password. Detail: {message}", ex.Message);
                throw;
            }
        }

        public async Task UpdateUserName(int id, string fullName)
        {
            try
            {
                _logger.LogInformation("Starting to update user name");

                await _userInfoProvider.UpdateUserFullName(id, fullName);

                _logger.LogInformation("Udpating user name finished");
            }
            catch (Exception ex)
            {
                _logger.LogError("Something wrong happen. Cannot change the user name. Detail: {message}", ex.Message);
                throw;
            }
        }
    }
}