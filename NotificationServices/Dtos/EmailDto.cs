﻿using System.ComponentModel.DataAnnotations;

namespace NotificationServices.Api.Dtos
{
    public class EmailDto
    {
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$")]
        public string To { get; set; } = string.Empty;

        public string Subject { get; set; } = string.Empty;

        public string Body { get; set; } = string.Empty;
    }
}