﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NotificationServices.Api.Dtos;
using NotificationServices.Core.Interfaces.API;
using NotificationServices.Core.Request;
using System.Diagnostics;

namespace NotificationServices.Api.Controllers.v2
{
    [ApiController]
    [Route("api/v{version:apiVersion}/mails")]
    [ApiVersion("2.0")]
    public class EmailController : ControllerBase
    {
        private readonly IEmaiServiceManager _emailManager;
        private readonly IMapper _mapper;
        private readonly ILogger<EmailController> _logger;

        public EmailController(IEmaiServiceManager emailManager, IMapper mapper, ILogger<EmailController> logger)
        {
            _emailManager = emailManager;
            _mapper = mapper;
            _logger = logger;
        }

        [MapToApiVersion("2.0")]
        [HttpPost("recipients")]
        public async Task<IActionResult> SendEmailToRecipientsAsync(List<EmailDto> requestDtos)
        {
            _logger.LogInformation($"Start sending mail to {requestDtos.Count}email addresses");
            Stopwatch sw = Stopwatch.StartNew();

            var request = _mapper.Map<List<EmailDto>, List<SendMailRequest>>(requestDtos);
            await _emailManager.SendEmailToRecipients(request);

            sw.Stop();
            _logger.LogInformation($"Sending mail to {requestDtos.Count} finished in {sw.ElapsedMilliseconds} ms");

            return Ok();
        }
    }
}