﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NotificationServices.Api.Dtos;
using NotificationServices.Core.Interfaces.API;
using NotificationServices.Core.Request;
using System.Diagnostics;

namespace NotificationServices.Api.Controllers.v1
{
    [ApiController]
    [Route("api/v{version:apiVersion}/mails")]
    [ApiVersion("1.0")]
    public class EmailController : ControllerBase
    {
        private readonly IEmaiServiceManager _emailManager;
        private readonly IMapper _mapper;
        private readonly ILogger<EmailController> _logger;

        public EmailController(IEmaiServiceManager emailManager, IMapper mapper, ILogger<EmailController> logger)
        {
            _emailManager = emailManager;
            _mapper = mapper;
            _logger = logger;
        }

        [MapToApiVersion("1.0")]
        [HttpPost("recipient")]
        [Consumes("application/json")]
        public async Task<IActionResult> SendEmailAsync(EmailDto requestDto)
        {
            _logger.LogInformation($"Start sending mail to email address: {requestDto.To}");
            Stopwatch sw = Stopwatch.StartNew();

            var request = _mapper.Map<EmailDto, SendMailRequest>(requestDto);
            await _emailManager.SendEmailAsync(request);

            sw.Stop();
            _logger.LogInformation($"Sending mail finished in {sw.ElapsedMilliseconds} ms");

            return Ok();
        }
    }
}