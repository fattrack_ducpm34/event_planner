﻿using AutoMapper;
using NotificationServices.Api.Dtos;
using NotificationServices.Core.Request;

namespace NotificationServices.Api.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<EmailDto, SendMailRequest>().ReverseMap();
        }
    }
}