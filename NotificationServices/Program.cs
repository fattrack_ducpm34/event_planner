using Consul;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using NotificationServices.Api.Mapper;
using NotificationServices.Api.Middleware;
using NotificationServices.Api.Options;
using NotificationServices.Core.Interfaces.API;
using NotificationServices.Core.Interfaces.SPI;
using NotificationServices.Core.Services;
using NotificationServices.Infrastructure.Services;

namespace NotificationServices.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            // Add services to the container.
            builder.Services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder =>
                {
                    builder.AllowAnyOrigin()
                           .AllowAnyHeader()
                           .AllowAnyMethod();
                });
            });

            // API VERSIONING
            builder.Services.ConfigureOptions<ConfigureSwaggerOptions>();

            builder.Services.AddApiVersioning(opt =>
            {
                opt.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
                opt.AssumeDefaultVersionWhenUnspecified = true;
                opt.ReportApiVersions = true;
                opt.ApiVersionReader = new UrlSegmentApiVersionReader();
            });

            builder.Services.AddVersionedApiExplorer(setup =>
            {
                setup.GroupNameFormat = "'v'VVV";
                setup.SubstituteApiVersionInUrl = true;
            });

            // DI
            builder.Services.AddAutoMapper(typeof(MappingProfile));
            builder.Services.AddScoped<IEmailServiceProvider, EmailServiceProvider>();
            builder.Services.AddScoped<IEmaiServiceManager, EmailServiceManager>();
            builder.Services.AddTransient<GlobalExceptionHandlerMiddleware>();

            // Consul service
            builder.Services.AddSingleton<IConsulClient, ConsulClient>(p => new ConsulClient(consulConfig =>
            {
                consulConfig.Address = new Uri("http://127.0.0.1:8500");
            }));

            //RabbitMq
            builder.Services.AddSingleton<ISendMailEventProcessor, SendMailEventProcessor>();
            builder.Services.AddHostedService<SendMailMessageSubscriber>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {
                    var provider = app.Services.GetRequiredService<IApiVersionDescriptionProvider>();
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.ApiVersion.ToString());
                    }
                });
            }
            app.UseConsul();

            app.UseCors("CorsPolicy");

            app.UseMiddleware<GlobalExceptionHandlerMiddleware>();

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}