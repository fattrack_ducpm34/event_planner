﻿using EventPlannerServices.Core.Helper;
using Xunit;

namespace EventPlannerServices.Test.Core.Helper
{
    public class EmailValidatorTests
    {
        [Theory]
        [InlineData("test@example.com", true)]           // Valid email
        [InlineData("user123@gmail.com", true)]         // Valid email
        [InlineData("john.doe@company.co.uk", true)]    // Valid email
        [InlineData("invalid-email", false)]            // Invalid email (no '@' symbol)
        [InlineData("user@invalid-domain", false)]      // Invalid email (invalid domain)
        [InlineData("", false)]                         // Empty email
        [InlineData(null, false)]                       // Null email

        public void IsValidEmail_ShouldReturnExpectedResult(string email, bool expectedResult)
        {
            // Act
            bool result = EmailValidator.IsValidEmail(email);

            // Assert
            Assert.Equal(expectedResult, result);
        }
    }
}
