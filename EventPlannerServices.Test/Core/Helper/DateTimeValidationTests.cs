﻿using EventPlannerServices.Core.Helper;
using Xunit;

namespace EventPlannerServices.Test.Core.Helper
{
    public class DateTimeValidationTests
    {
        [Theory]
        [InlineData("2024-01-01T08:00:00", "2024-01-01T09:00:00", true)] // Valid inputs
        [InlineData("2024-01-01T10:00:00", "2024-01-01T09:00:00", false)] // EndTime before StartTime
        [InlineData("2024-01-01T12:00:00", "2024-01-01T19:00:00", false)] // EndTime after validEndTime
        [InlineData("2024-01-01T10:00:00", "2024-01-01T10:15:00", false)] // Not multiple of 30 minutes
        [InlineData("2024-01-01T10:00:00", "2024-01-02T10:00:00", false)] // Different dates
        public void ValidateDateTime_ShouldReturnExpectedResult(
            string startDateTimeStr, string endDateTimeStr, bool expectedResult)
        {
            // Arrange
            DateTime startTime = DateTime.Parse(startDateTimeStr);
            DateTime endTime = DateTime.Parse(endDateTimeStr);

            // Act
            bool result = DateTimeValidation.ValidateDateTime(startTime, endTime);

            // Assert
            Assert.Equal(expectedResult, result);
        }
    }
}