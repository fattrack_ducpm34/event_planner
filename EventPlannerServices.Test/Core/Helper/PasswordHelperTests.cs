﻿using EventPlannerServices.Core.Helper;
using Xunit;

namespace EventPlannerServices.Test.Core.Helper
{
    public class PasswordHelperTests
    {
        [Fact]
        public void HashPassword_ValidInput_ShouldNotBeNull()
        {
            // Arrange
            string password = "TestPassword";

            // Act
            string hashedPassword = PasswordHelper.HashPassword(password);

            // Assert
            Assert.NotNull(hashedPassword);
        }

        [Fact]
        public void VerifyPassword_CorrectPassword_ShouldReturnTrue()
        {
            // Arrange
            string password = "TestPassword";
            string hashedPassword = PasswordHelper.HashPassword(password);

            // Act
            bool result = PasswordHelper.VerifyPassword(password, hashedPassword);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void VerifyPassword_IncorrectPassword_ShouldReturnFalse()
        {
            // Arrange
            string correctPassword = "TestPassword";
            string incorrectPassword = "IncorrectPassword";
            string hashedPassword = PasswordHelper.HashPassword(correctPassword);

            // Act
            bool result = PasswordHelper.VerifyPassword(incorrectPassword, hashedPassword);

            // Assert
            Assert.False(result);
        }

        [Theory]
        [InlineData("WeakPass", "Minimum password length should be 8")]
        [InlineData("Anhduc1711@", "")]
        [InlineData("1234567891", "Password should be AlphaNumeric")]
        [InlineData("AlphaNumeric123", "Password should contain special charcter")]
        public void CheckPasswordStrength_ShouldReturnExpectedResult(string password, string expectedResult)
        {
            // Act
            string result = PasswordHelper.CheckPasswordStrength(password);

            // Assert
            Assert.Contains(expectedResult, result);
        }
    }
}