﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Core.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace EventPlannerServices.Test.Core.Services
{
    public class RoleManagerTests
    {
        [Fact]
        public async Task AddNewRole_Success()
        {
            // Arrange
            var loggerMock = new Mock<ILogger<RoleManager>>();
            var roleRepositoryMock = new Mock<IGenericRepository<Role>>();

            var roleManager = new RoleManager(loggerMock.Object, roleRepositoryMock.Object);

            var role = new Role { /* set properties */ };

            // Act
            await roleManager.AddNewRole(role);

            // Assert
            roleRepositoryMock.Verify(repo => repo.Create(role), Times.Once);
        }

        [Fact]
        public async Task AddNewRole_Fail()
        {
            // Arrange
            var loggerMock = new Mock<ILogger<RoleManager>>();
            var roleRepositoryMock = new Mock<IGenericRepository<Role>>();

            var roleManager = new RoleManager(loggerMock.Object, roleRepositoryMock.Object);

            var role = new Role { /* set properties */ };

            roleRepositoryMock.Setup(repo => repo.Create(role)).ThrowsAsync(new Exception("Create role failed"));

            // Act và Assert
            await Assert.ThrowsAsync<Exception>(() => roleManager.AddNewRole(role));
        }
    }
}