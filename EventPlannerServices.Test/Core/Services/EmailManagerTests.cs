﻿using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Core.Request;
using EventPlannerServices.Core.Services;
using Moq;
using Xunit;

namespace EventPlannerServices.Test.Core.Services
{
    public class EmailManagerTests
    {
        [Fact]
        public void PublishNewSendMailRequest_Success()
        {
            // Arrange
            var emailProviderMock = new Mock<IEmailProvider>();
            var emailManager = new EmailManager(emailProviderMock.Object);

            var sendMailRequests = new List<SendMailRequest>
            {
                new SendMailRequest { /* set properties */ },
                // Add more SendMailRequest objects as needed
            };

            // Act
            emailManager.PublishNewSendMailRequest(sendMailRequests);

            // Assert
            emailProviderMock.Verify(provider => provider.PublishNewSendMailRequest(sendMailRequests), Times.Once);
        }

        [Fact]
        public async Task SendMail_Success()
        {
            // Arrange
            var emailProviderMock = new Mock<IEmailProvider>();
            var emailManager = new EmailManager(emailProviderMock.Object);

            var sendMailRequest = new SendMailRequest { /* set properties */ };

            // Act
            await emailManager.SendMail(sendMailRequest);

            // Assert
            emailProviderMock.Verify(provider => provider.SendMail(sendMailRequest), Times.Once);
        }
    }
}