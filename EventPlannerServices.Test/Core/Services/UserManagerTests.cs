﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Core.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace EventPlannerServices.Test.Core.Services
{
    public class UserManagerTests
    {
        private readonly Mock<ILogger<UserManager>> loggerMock;
        private readonly Mock<IGenericRepository<User>> userRepositoryMock;
        private readonly Mock<IUserProvider> userProviderMock;
        private readonly Mock<IUserRoleManager> userRoleManagerMock;
        private UserManager userManager;

        public UserManagerTests()
        {
            loggerMock = new Mock<ILogger<UserManager>>();
            userRepositoryMock = new Mock<IGenericRepository<User>>();
            userProviderMock = new Mock<IUserProvider>();
            userRoleManagerMock = new Mock<IUserRoleManager>();

            userManager = new UserManager(loggerMock.Object, userRepositoryMock.Object, userProviderMock.Object, userRoleManagerMock.Object);
        }

        [Fact]
        public async Task DeleteUser_Success()
        {
            // Arrange
            var userId = 1;

            // Act
            await userManager.DeleteUser(userId);

            // Assert
            userRepositoryMock.Verify(repo => repo.Delete(userId), Times.Once);
        }

        [Fact]
        public async Task GetUserByUserName_Success()
        {
            // Arrange
            var userName = "testUser";

            var expectedUser = new User { Id = 1, Username = userName, /* other properties */ };
            userProviderMock.Setup(provider => provider.GetUserByUserName(userName)).ReturnsAsync(expectedUser);

            // Act
            var resultUser = await userManager.GetUserByUserName(userName);

            // Assert
            Assert.NotNull(resultUser);
            Assert.Equal(expectedUser, resultUser);
        }

        [Fact]
        public async Task GetUserLogin_Success()
        {
            // Arrange
            var userName = "testUser";
            var password = "testPassword";

            var expectedUser = new User { Id = 1, Username = userName, Password = password, /* other properties */ };
            userProviderMock.Setup(provider => provider.GetUserLogin(userName, password)).ReturnsAsync(expectedUser);

            // Act
            var resultUser = await userManager.GetUserLogin(userName, password);

            // Assert
            Assert.NotNull(resultUser);
            Assert.Equal(expectedUser, resultUser);
        }

        [Fact]
        public async Task UpdateUser_Success()
        {
            // Arrange
            var user = new User { Id = 1, Username = "testUser", /* other properties */ };

            // Act
            await userManager.UpdateUser(user);

            // Assert
            userProviderMock.Verify(provider => provider.UpdateUser(user), Times.Once);
        }

        [Fact]
        public async Task AddUser_Fail()
        {
            // Arrange
            var user = new User { /* set user properties */ };

            userProviderMock.Setup(provider => provider.AddUser(user)).ThrowsAsync(new Exception("AddUser failed"));

            // Act and Assert
            await Assert.ThrowsAsync<Exception>(() => userManager.AddUser(user));
        }

        [Fact]
        public async Task DeleteUser_Fail()
        {
            // Arrange
            var userId = 1;

            userRepositoryMock.Setup(repo => repo.Delete(userId)).ThrowsAsync(new Exception("Delete failed"));

            // Act and Assert
            await Assert.ThrowsAsync<Exception>(() => userManager.DeleteUser(userId));
        }
    }
}