﻿using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Core.Request;
using EventPlannerServices.Core.Responses;
using EventPlannerServices.Core.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace EventPlannerServices.Test.Core.Services
{
    public class BookingManagerTests
    {
        private readonly Mock<ILogger<BookingManager>> loggerMock;
        private readonly Mock<IBookingProvider> bookingProviderMock;
        private readonly Mock<IEmailManager> emailManagerMock;
        private readonly Mock<IRoomProvider> roomProviderMock;
        private readonly Mock<IUserProvider> userProviderMock;
        private BookingManager bookingManager;

        public BookingManagerTests()
        {
            loggerMock = new Mock<ILogger<BookingManager>>();
            bookingProviderMock = new Mock<IBookingProvider>();
            emailManagerMock = new Mock<IEmailManager>();
            roomProviderMock = new Mock<IRoomProvider>();
            userProviderMock = new Mock<IUserProvider>();

            bookingManager = new BookingManager(loggerMock.Object, bookingProviderMock.Object, emailManagerMock.Object, roomProviderMock.Object, userProviderMock.Object);
        }

        [Fact]
        public async Task AddBooking_Success()
        {
            // Arrange
            userProviderMock.Setup(x => x.GetUserById(It.IsAny<int>())).ReturnsAsync(new User{ Email = "Test@Test.com"});
            roomProviderMock.Setup(x => x.GetRoomById(It.IsAny<int>())).Returns(new Room { Location = "Duy Tan", Floor = 10 });

            DateTime startDateTime = new DateTime(2024, 1, 20, 8, 0, 0);
            DateTime endDateTime = new DateTime(2024, 1, 20, 9, 0, 0);

            // Act
            await bookingManager.AddBooking(1, 2, startDateTime, endDateTime, "BookingName", "BookingDescription", "attendeesList");

            // Assert
            bookingProviderMock.Verify(provider => provider.AddBooking(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            emailManagerMock.Verify(manager => manager.PublishNewSendMailRequest(It.IsAny<List<SendMailRequest>>()), Times.Once);
            roomProviderMock.Verify(provider => provider.GetRoomById(It.IsAny<int>()), Times.Once);
            userProviderMock.Verify(provider => provider.GetUserById(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task AddBooking_InvalidTimeframe_Exception()
        {
            // Arrange
            userProviderMock.Setup(x => x.GetUserById(It.IsAny<int>())).ReturnsAsync(new User { Email = "Test@Test.com" });
            roomProviderMock.Setup(x => x.GetRoomById(It.IsAny<int>())).Returns(new Room { Location = "Duy Tan", Floor = 10 });
            bookingProviderMock.Setup(provider => provider.AddBooking(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));

            // Act and Assert
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await bookingManager.AddBooking(1, 2, DateTime.UtcNow, DateTime.UtcNow.AddHours(-1), "BookingName", "BookingDescription", "attendeesList");
            });

            // Assert
            bookingProviderMock.Verify(provider => provider.AddBooking(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            emailManagerMock.Verify(manager => manager.PublishNewSendMailRequest(It.IsAny<List<SendMailRequest>>()), Times.Never);
            roomProviderMock.Verify(provider => provider.GetRoomById(It.IsAny<int>()), Times.Never);
            userProviderMock.Verify(provider => provider.GetUserById(It.IsAny<int>()), Times.Never);
        }

        [Fact]
        public async Task CancelReservation_Success()
        {
            // Arrange
            var booking = new Booking
            {
                AttendeesList = "email@email.com",
                Name = "Booking Name",
                StartTime = DateTime.UtcNow,
                EndTime = DateTime.UtcNow.AddMinutes(30),
                Description = "Booking Description",
                User = new User
                {
                    Email = "UserEmail@email.com"
                },
                Room = new Room
                {
                    Location = "Duy Tan",
                    Floor = 10
                }
            };

            bookingProviderMock.Setup(x => x.CancelReservation(It.IsAny<int>(), It.IsAny<int>())).ReturnsAsync(booking);

            // Act
            await bookingManager.CancelReservation(1, 2);

            // Assert
            bookingProviderMock.Verify(provider => provider.CancelReservation(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            emailManagerMock.Verify(manager => manager.PublishNewSendMailRequest(It.IsAny<List<SendMailRequest>>()), Times.Once);
        }

        [Fact]
        public async Task GetBookingInfoDetail_Success()
        {
            // Act
            await bookingManager.GetBookingInfoDetail(1);

            // Assert
            bookingProviderMock.Verify(provider => provider.GetBookingsDetail(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task GetBookingsInfo_Success()
        {
            // Act
            await bookingManager.GetBookingsInfo(new GetBookingInfoRequest());

            // Assert
            bookingProviderMock.Verify(provider => provider.GetBookingsInfo(It.IsAny<GetBookingInfoRequest>()), Times.Once);
        }

        [Fact]
        public async Task UpdateReservation_Success()
        {
            // Assert
            // Arrange
            var booking = new Booking
            {
                AttendeesList = "attendeesList",
                Name = "BookingName",
                StartTime = DateTime.UtcNow,
                EndTime = DateTime.UtcNow.AddHours(2),
                Description = "BookingDescription",
                User = new User
                {
                    Email = "UserEmail@email.com"
                },
                Room = new Room
                {
                    Location = "Duy Tan",
                    Floor = 10
                }
            };

            DateTime startDateTime = new DateTime(2024, 1, 20, 8, 0, 0);
            DateTime endDateTime = new DateTime(2024, 1, 20, 9, 0, 0);

            bookingProviderMock.Setup(x => x.UpdateReservation(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(booking);

            // Act
            await bookingManager.UpdateReservation(It.IsAny<int>(), It.IsAny<int>(), startDateTime, endDateTime, "BookingName", "BookingDescription", "attendeesList");

            // Assert
            bookingProviderMock.Verify(provider => provider.UpdateReservation(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            emailManagerMock.Verify(manager => manager.PublishNewSendMailRequest(It.IsAny<List<SendMailRequest>>()), Times.Once);
        }

        [Fact]
        public async Task UpdateReservation_InvalidTimeframe_Exception()
        {
            // Act and Assert
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await bookingManager.UpdateReservation(1, 2, DateTime.UtcNow, DateTime.UtcNow.AddHours(-1), "BookingName", "BookingDescription", "attendeesList");
            });

            // Assert
            bookingProviderMock.Verify(provider => provider.UpdateReservation(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            emailManagerMock.Verify(manager => manager.PublishNewSendMailRequest(It.IsAny<List<SendMailRequest>>()), Times.Never);
        }

        [Fact]
        public async Task GetAvailableRooms_Success()
        {
            // Assert 
            var avaiableRooms = new List<AvaiableRoomResponse>()
            {
                new AvaiableRoomResponse
                {
                    Id = 1,
                    Capacity = 10,
                    Location = "Duy Tan",
                    Floor = 10,
                    Name = "Happy",
                    Description = "Happy is Happy",
                    RoomType = "meeting room",
                    Status = "ready for book"
                },
                new AvaiableRoomResponse
                {
                    Id = 2,
                    Capacity = 10,
                    Location = "Duy Tan",
                    Floor = 10,
                    Name = "Happy",
                    Description = "Happy is Happy",
                    RoomType = "banquet hall",
                    Status = "ready for book"
                }
            };

            bookingProviderMock.Setup(x => x.GetAvailableRooms(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>())).ReturnsAsync(avaiableRooms);
            // Act
            await bookingManager.GetAvailableRooms(DateTime.UtcNow, DateTime.UtcNow.AddHours(1), "Location", 10, 1);

            // Assert
            bookingProviderMock.Verify(provider => provider.GetAvailableRooms(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
        }
    }
}