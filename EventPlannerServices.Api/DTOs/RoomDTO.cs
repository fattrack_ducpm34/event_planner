﻿namespace EventPlannerServices.Api.DTOs
{
    public class RoomDTO
    {
        public int RoomId { get; set; }
        public string? RoomName { get; set; }
        public int? RoomCapacity { get; set; }
        public string? RoomDescription { get; set; }
        public string? RoomLocation { get; set; }
        public int? RoomFloor { get; set; }
        public int? RoomTypeId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int? RoomStatusId { get; set; }
    }
}