﻿namespace EventPlannerServices.Api.DTOs
{
    public class SearchRoomParameters
    {
        public string? Location { get; set; }
        public string? RoomName { get; set; }
        public int? RoomFloor { get; set; }
        public int? RoomStatus { get; set; }
        public int? RoomType { get; set; }
        public DateTime? Date { get; set; }
        public int? Capacity { get; set; }
    }
}
