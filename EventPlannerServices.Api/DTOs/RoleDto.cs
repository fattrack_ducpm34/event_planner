﻿namespace EventPlannerServices.Api.DTOs
{
    public class RoleDto
    {
        public string Code { get; set; }

        public string Description { get; set; }
    }
}
