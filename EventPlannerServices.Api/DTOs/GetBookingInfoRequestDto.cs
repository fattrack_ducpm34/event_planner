﻿namespace EventPlannerServices.Api.DTOs
{
    public class GetBookingInfoRequestDto
    {
        public string Location { get; set; } = string.Empty;
        public string UserId { get; set; } = string.Empty;
        public DateTime Date { get; set; }
        public string RoomTypeId { get; set; } = string.Empty;
        public string RoomName { get; set; } = string.Empty;
        public string Capacity { get; set; } = string.Empty;
    }
}
