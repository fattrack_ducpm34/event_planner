﻿namespace EventPlannerServices.Api.DTOs
{
    public class UserDto
    {
        public string Username { get; set; } = null!;

        public string Password { get; set; } = null!;

        public string ConfirmedPassword { get; set; } = null!;

        public string Email { get; set; } = null!;
    }
}
