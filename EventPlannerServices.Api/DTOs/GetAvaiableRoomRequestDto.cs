﻿namespace EventPlannerServices.Api.DTOs
{
    public class GetAvaiableRoomRequestDto
    {
        public DateTime BeginTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Location { get; set; } = string.Empty;
        public int Capacity { get; set; }
        public int RoomTypeId { get; set; }
    }
}
