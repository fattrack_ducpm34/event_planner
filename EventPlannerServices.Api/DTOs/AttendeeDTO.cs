﻿namespace EventPlannerServices.Api.DTOs
{
    public class AttendeeDTO
    {
        public int AttendeeId { get; set; }
        public string AttendeeName { get; set; }
        public string AttendeeEmail { get; set; }
        public string AttendeePhone { get; set; }
        public string AttendeeCompany { get; set; }
        public string AttendeeTitle { get; set; }
        public string AttendeeDescription { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}
