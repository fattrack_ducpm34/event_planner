﻿namespace EventPlannerServices.Api.DTOs
{
    public class AddBookingDto
    {
        //int userId, int roomId, DateTime startTime, DateTime endTime, string bookingName, string bookingDescription

        public int UserId { get; set; }
        public int RoomId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string BookingName { get; set; } = string.Empty;
        public string BookingDescription { get; set; } = string.Empty;
        public string? AttendeesList { get; set; } = string.Empty;
    }
}
