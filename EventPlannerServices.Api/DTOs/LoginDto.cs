﻿namespace EventPlannerServices.Api.DTOs
{
    public class LoginDto
    {
        // FOR DEFAULT ADMIN USER ACCOUNT : Admin/Anhduc1711@
        public string UserName { get; set; }

        // FOR DEFAULT ADMIN USER ACCOUNT : Admin/Anhduc1711@
        public string Password { get; set; }
    }
}
