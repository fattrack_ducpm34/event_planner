﻿namespace EventPlannerServices.Api.DTOs
{
    public class UserRoleDto
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
