﻿namespace EventPlannerServices.Api.DTOs
{
    public class ImageDto
    {
        public string? Name { get; set; }

        public byte[]? ImageData { get; set; }
    }
}