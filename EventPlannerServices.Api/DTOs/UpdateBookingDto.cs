﻿namespace EventPlannerServices.Api.DTOs
{
    public class UpdateBookingDto
    {
        public int BookingId { get; set; }
        public int UserId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string BookingName { get; set; }
        public string BookingDescription { get; set; }
        public string? AttendeesList { get; set; } = string.Empty;
    }
}
