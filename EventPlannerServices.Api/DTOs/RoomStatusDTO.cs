﻿namespace EventPlannerServices.Api.DTOs
{
    public class RoomStatusDTO
    {
        public int Id { get; set; }
        public string? Status { get; set; }
        public string? StatusDescription { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
