﻿using AutoMapper;
using EventPlannerServices.Api.DTOs;
using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EventPlannerServices.Api.Controllers
{
    [Route("api/v{version:apiVersion}/images")]
    [ApiController]
    [ApiVersion("1.0")]
    public class ImageController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IImageManager _imageManager;

        public ImageController(IMapper mapper, IImageManager imageManager)
        {
            _mapper = mapper;
            _imageManager = imageManager;
        }

        [HttpPost]
        [Route("UploadImage")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UploadImage([FromForm] IFormFile file)
        {
            try
            {
                ImageDto img = new ImageDto();

                if (file != null && file.Length > 0)
                {
                    img.Name = file.FileName;

                    using (MemoryStream ms = new MemoryStream())
                    {
                        file.CopyTo(ms);
                        img.ImageData = ms.ToArray();
                    }

                    await _imageManager.AddImage(_mapper.Map<Image>(img));

                    return Ok("Image stored in database!");
                }
                else
                {
                    return BadRequest("No image file provided");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpGet]
        [Route("GetImage/{id}")]
        [Authorize]
        public async Task<IActionResult> GetImage(int id)
        {
            try
            {
                var img = await _imageManager.LoadImage(id);

                if (img != null)
                {
                    return Ok(Convert.ToBase64String(img.ImageData));
                }
                else
                {
                    return NotFound("Image not found");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }
    }
}