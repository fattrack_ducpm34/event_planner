﻿using EventPlannerServices.Api.DTOs;
using EventPlannerServices.Core.Interfaces.API;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EventPlannerServices.Api.Controllers
{
    [Route("api/v{version:apiVersion}/userinfos")]
    [ApiController]
    [Authorize]
    [ApiVersion("1.0")]
    public class UserInfoController : ControllerBase
    {
        private readonly IUserInfoManager _userInfoManager;

        public UserInfoController(IUserInfoManager userInfoManager)
        {
            _userInfoManager = userInfoManager;
        }

        [HttpPost("fullname")]
        [MapToApiVersion("1.0")]
        public async Task<IActionResult> UpdateUserFullName([FromBody] string fullName)
        {
            HttpContext context = HttpContext;
            string userId = "";

            if (context.User.Identity.IsAuthenticated)
            {
                userId = context.User.FindFirst("Id")?.Value;

                if (string.IsNullOrEmpty(userId))
                {
                    return BadRequest("Can not get id from token");
                }
            }

            if (fullName == null)
            {
                return BadRequest("Invalid request");
            }

            await _userInfoManager.UpdateUserName(int.Parse(userId), fullName);

            return Ok();
        }

        [HttpPost("change-password")]
        [MapToApiVersion("1.0")]
        public async Task<IActionResult> ChangePassword([FromBody] UpdatePasswordDto updatePasswordDto)
        {
            HttpContext context = HttpContext;
            string userId = "";

            if (context.User.Identity.IsAuthenticated)
            {
                userId = context.User.FindFirst("Id")?.Value;

                if (string.IsNullOrEmpty(userId))
                {
                    return BadRequest("Can not get id from token");
                }
            }

            if (updatePasswordDto.NewPassword != updatePasswordDto.ConfirmedPassword)
            {
                return BadRequest("Password does not match");
            }

            await _userInfoManager.ChangePassword(int.Parse(userId), updatePasswordDto.OldPassword, updatePasswordDto.NewPassword);

            return Ok();
        }
    }
}