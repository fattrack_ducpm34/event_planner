﻿using AutoMapper;
using EventPlannerServices.Api.DTOs;
using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Request;
using Microsoft.AspNetCore.Mvc;

namespace EventPlannerServices.Api.Controllers
{
    [Route("api/v{version:apiVersion}/login")]
    [ApiController]
    [ApiVersion("1.0")]
    public class LoginController : Controller
    {
        private readonly IUserManager _userManager;
        private readonly ITokenService _tokenService;
        private readonly IMapper _mapper;
        private readonly IEmailManager _emailManager;

        public LoginController(IUserManager userManager, ITokenService tokenService, IMapper mapper, IEmailManager emailManager)
        {
            _userManager = userManager;
            _tokenService = tokenService;
            _mapper = mapper;
            _emailManager = emailManager;
        }

        /// <summary>
        /// FOR DEFAULT ADMIN USER ACCOUNT : Admin/Anhduc1711@
        /// </summary>
        /// <param name="loginDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("authenticate")]
        [MapToApiVersion("1.0")]
        public async Task<IActionResult> Authenticate([FromBody] LoginDto loginDto)
        {
            if (loginDto == null)
                return BadRequest();

            var user = await _userManager.GetUserLogin(loginDto.UserName, loginDto.Password);

            user.Token = await _tokenService.CreateJwt(user);
            var newAccessToken = user.Token;
            var newRefreshToken = _tokenService.CreateRefreshToken(user);
            user.RefreshToken = newRefreshToken;
            user.RefreshTokenExpiryTime = DateTime.Now.AddMinutes(60);
            await _userManager.UpdateUser(user);

            return Ok(new TokenApiDto()
            {
                AccessToken = newAccessToken,
                RefreshToken = newRefreshToken
            });
        }

        /// <summary>
        /// Sign up
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        [HttpPost("sign-up")]
        [MapToApiVersion("1.0")]
        public async Task<IActionResult> SignUp([FromBody] UserDto userDto)
        {
            try
            {
                if (userDto.Password != userDto.ConfirmedPassword)
                {
                    throw new ArgumentException("password and confirm password does not match!!!");
                }

                var request = _mapper.Map<UserDto, User>(userDto);

                await _userManager.AddUser(request);

                var notificationRequest = new SendMailRequest
                {
                    To = request.Email ?? "",
                    Subject = "Welcome to Event Planner of LTS",
                    Body = $"Hello {userDto.Username},\n\n" +
                   "Thank you for signing up with our Event Planner App." +
                   "You can now start booking meeting rooms and managing your reservations.\n\n" +
                   "Best regards,\nLTS Team"
                };

                await _emailManager.SendMail(notificationRequest);

                return Ok();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Just for my test.
        /// </summary>
        /// <param name="tokenApiDto"></param>
        /// <returns></returns>
        [HttpPost("refresh")]
        [MapToApiVersion("1.0")]
        public async Task<IActionResult> Refresh([FromBody] TokenApiDto tokenApiDto)
        {
            if (tokenApiDto is null)
                return BadRequest("Invalid Client Request");

            string accessToken = tokenApiDto.AccessToken;
            string refreshToken = tokenApiDto.RefreshToken;
            var principal = await _tokenService.GetPrincipleFromExpiredToken(accessToken);
            var username = principal.Identity.Name;
            var user = await _userManager.GetUserByUserName(username);
            if (user is null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.Now)
                return BadRequest("Invalid Request");
            var newAccessToken = await _tokenService.CreateJwt(user);
            var newRefreshToken = _tokenService.CreateRefreshToken(user);
            user.RefreshToken = newRefreshToken;

            await _userManager.UpdateUser(user);

            return Ok(new TokenApiDto()
            {
                AccessToken = newAccessToken,
                RefreshToken = newRefreshToken,
            });
        }
    }
}