﻿using AutoMapper;
using EventPlannerServices.Api.DTOs;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Request;
using Microsoft.AspNetCore.Mvc;

namespace EventPlannerServices.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DemoSendMail : ControllerBase
    {
        private readonly IEmailManager _emailManager;
        private readonly IMapper _mapper;

        public DemoSendMail(IEmailManager emailManager, IMapper mapper)
        {
            _emailManager = emailManager;
            _mapper = mapper;
        }

        [Route("test")]
        [HttpGet]
        public async Task<IActionResult> testapigateway()
        {
            //var url = "http://localhost:5071/api/v1/mails/recipient";

            var body = new EmailDto
            {
                To = "duc34@yopmail.com",
                Subject = "Test rest sharp",
                Body = "Test rest sharp"
            };

            var body1 = new EmailDto
            {
                To = "duc34@yopmail.com",
                Subject = "Test rabbitmq",
                Body = "Test rabbitmq"
            };

            List<SendMailRequest> listRequest = new List<SendMailRequest>()
            {
               _mapper.Map<SendMailRequest>(body),
               _mapper.Map<SendMailRequest>(body1)
            };

            _emailManager.PublishNewSendMailRequest(listRequest);

            //Using API Gateway
            //var request = _mapper.Map<SendMailRequest>(body);
            //await _emailManager.SendMail(request);

            return Ok();
        }
    }
}