﻿using AutoMapper;
using EventPlannerServices.Api.Attributes;
using EventPlannerServices.Api.DTOs;
using EventPlannerServices.Api.Redis;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Request;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EventPlannerServices.Api.Controllers
{
    [Route("api/v{version:apiVersion}/booking")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize]
    public class BookingController : ControllerBase
    {
        private readonly IBookingManager _bookingManager;
        private readonly IMapper _mapper;
        private readonly IResponseCacheService _responseCacheService;

        public BookingController(IBookingManager bookingManager, IMapper mapper, IResponseCacheService responseCacheService)
        {
            _bookingManager = bookingManager;
            _mapper = mapper;
            _responseCacheService = responseCacheService;
        }

        [HttpPost]
        [MapToApiVersion("1.0")]
        public async Task<IActionResult> AddBooking([FromBody] AddBookingDto addBookingDto)
        {
            try
            {
                await _responseCacheService.RemoveCacheResponseAsync($"/api/v1/booking/search");

                await _bookingManager.AddBooking(addBookingDto.UserId, addBookingDto.RoomId, addBookingDto.StartTime, addBookingDto.EndTime, addBookingDto.BookingName, addBookingDto.BookingDescription, addBookingDto.AttendeesList);

                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal Server Error");
            }
           
        }

        [HttpGet]
        [Route("search")]
        [MapToApiVersion("1.0")]
        [Cache(1000)]
        public async Task<IActionResult> GetBookingInfo([FromQuery] GetBookingInfoRequestDto requestDto)
        {
            var request = _mapper.Map<GetBookingInfoRequest>(requestDto);
            var result = await _bookingManager.GetBookingsInfo(request);
            return Ok(result);
        }

        [HttpGet]
        [Route("search/{bookingId}")]
        [MapToApiVersion("1.0")]
        [Cache(1000)]
        public async Task<IActionResult> GetBookingInfoDetail([FromRoute] int bookingId)
        {
            var result = await _bookingManager.GetBookingInfoDetail(bookingId);
            return Ok(result);
        }

        [HttpGet]
        [Route("search/avaiable-meetingroom")]
        [MapToApiVersion("1.0")]
        [Cache(1000)]
        public async Task<IActionResult> GetAvaiableMeetingRoom([FromQuery] GetAvaiableRoomRequestDto request)
        {
            var result = await _bookingManager.GetAvailableRooms(request.BeginTime, request.EndTime, request.Location, request.Capacity, request.RoomTypeId);
            return Ok(result);
        }

        [HttpPut]
        [Route("update")]
        [MapToApiVersion("1.0")]
        public async Task<IActionResult> UpdateReservation([FromBody] UpdateBookingDto updateBookingDto)
        {
            await _responseCacheService.RemoveCacheResponseAsync($"/api/v1/booking/search");
            await _bookingManager.UpdateReservation(updateBookingDto.BookingId, updateBookingDto.UserId, updateBookingDto.StartTime, updateBookingDto.EndTime, updateBookingDto.BookingName, updateBookingDto.BookingDescription, updateBookingDto.AttendeesList);
            return Ok();
        }

        [HttpDelete]
        [Route("delete/{bookingId}/{userId}")]
        [MapToApiVersion("1.0")]
        public async Task<IActionResult> CancelReservation([FromRoute] int bookingId, [FromRoute] int userId)
        {
            await _responseCacheService.RemoveCacheResponseAsync($"/api/v1/booking/search");
            await _bookingManager.CancelReservation(bookingId, userId);
            return Ok();
        }
    }
}