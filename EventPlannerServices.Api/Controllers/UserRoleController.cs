﻿using EventPlannerServices.Api.DTOs;
using EventPlannerServices.Core.Interfaces.API;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EventPlannerServices.Api.Controllers
{
    [Route("api/v{version:apiVersion}/roles")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    [ApiVersion("1.0")]
    public class UserRoleController : ControllerBase
    {
        private readonly IUserRoleManager _userRoleManger;

        public UserRoleController(IUserRoleManager userRoleManger)
        {
            _userRoleManger = userRoleManger;
        }

        [HttpPost("add")]
        [MapToApiVersion("1.0")]
        public async Task<IActionResult> AddRoleForUser([FromBody] UserRoleDto userRoleDto)
        {
            await _userRoleManger.AddRoleForUser(userRoleDto.UserId, userRoleDto.RoleId);

            return Ok();
        }

        [HttpDelete("delete")]
        [MapToApiVersion("1.0")]
        public async Task<IActionResult> RemoveRoleOfUser([FromBody] UserRoleDto userRoleDto)
        {
            await _userRoleManger.RemoveRoleOfUser(userRoleDto.UserId, userRoleDto.RoleId);

            return Ok();
        }
    }
}