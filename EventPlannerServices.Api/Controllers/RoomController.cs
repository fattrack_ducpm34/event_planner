﻿using AutoMapper;
using EventPlannerServices.Api.Attributes;
using EventPlannerServices.Api.DTOs;
using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using Microsoft.AspNetCore.Mvc;

namespace EventPlannerServices.Api.Controllers
{
    [Route("api/v{version:apiVersion}/rooms")]
    [ApiController]
    [ApiVersion("1.0")]
    public class RoomController : ControllerBase
    {
        private readonly ILogger<RoomController> _logger;
        private readonly IRoomManager _roomManager;
        private readonly IMapper _mapper;

        public RoomController(ILogger<RoomController> logger, IRoomManager roomManager, IMapper mapper)
        {
            _logger = logger;
            _roomManager = roomManager;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("locations")]
        [MapToApiVersion("1.0")]
        [Cache(1000)]
        public async Task<IActionResult> GetAllLocations()
        {
            try
            {
                var locations = await _roomManager.GetLocations();

                return Ok(locations);
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomController.GetLocations: Error getting all locations. Detail: {message}", ex.Message);
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpGet]
        [MapToApiVersion("1.0")]
        public IActionResult GetAllRooms()
        {
            try
            {
                var rooms = _roomManager.GetAllRooms();
                List<RoomDTO> roomDTOs = _mapper.Map<List<RoomDTO>>(rooms);

                return Ok(roomDTOs);
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomController.GetAllRooms: Error getting all rooms. Detail: {message}", ex.Message);
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpGet("{roomId}")]
        [MapToApiVersion("1.0")]
        public IActionResult GetRoomById(int roomId)
        {
            try
            {
                Room room = _roomManager.GetRoomById(roomId);

                if (room != null)
                {
                    var roomDTO = _mapper.Map<Room, RoomDTO>(room);

                    return Ok(roomDTO);
                }
                else
                {
                    return NotFound("Room not found");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomController.GetRoomById: Error getting room by ID. Detail: {message}", ex.Message);
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpGet("status/{statusId}")]
        [MapToApiVersion("1.0")]
        public IActionResult GetRoomsByStatus(int statusId)
        {
            try
            {
                var rooms = _roomManager.GetRoomsByStatus(statusId);
                List<RoomDTO> roomDTOs = _mapper.Map<List<RoomDTO>>(rooms);
                return Ok(roomDTOs);
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomController.GetRoomsByStatus: Error getting rooms by status. Detail: {message}", ex.Message);
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpPost]
        [MapToApiVersion("1.0")]
        public IActionResult AddRoom([FromBody] RoomDTO roomDTO)
        {
            try
            {
                Room room = _mapper.Map<RoomDTO, Room>(roomDTO);

                _roomManager.AddRoom(room);
                return Ok("Room added successfully");
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomController.AddRoom: Error adding room. Detail: {message}", ex.Message);
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpPut("{roomId}")]
        [MapToApiVersion("1.0")]
        public IActionResult UpdateRoom(int roomId, [FromBody] RoomDTO roomDTO)
        {
            try
            {
                Room room = _mapper.Map<RoomDTO, Room>(roomDTO);
                room.Id = roomId;

                _roomManager.UpdateRoom(room);
                return Ok("Room updated successfully");
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomController.UpdateRoom: Error updating room. Detail: {message}", ex.Message);
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpDelete("{roomId}")]
        [MapToApiVersion("1.0")]
        public IActionResult DeleteRoom(int roomId)
        {
            try
            {
                _roomManager.DeleteRoom(roomId);
                return Ok("Room deleted successfully");
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomController.DeleteRoom: Error deleting room. Detail: {message}", ex.Message);
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpGet("search")]
        [MapToApiVersion("1.0")]
        [Cache(1000)]
        public IActionResult SearchRoom(string? roomName, string? location, int? roomFloor = null, int? roomStatus = null, int? roomType = null, int? capacity = null)
        {
            try
            {
                var rooms = _roomManager.GetRoomsMatchConditions(roomName, location, roomFloor, roomStatus, roomType, capacity);
                List<RoomDTO> roomDTOs = _mapper.Map<List<RoomDTO>>(rooms);
                return Ok(roomDTOs);
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomController.GetRoomsMatchConditions: Error getting rooms with conditions. Detail: {message}", ex.Message);
                return StatusCode(500, "Internal Server Error");
            }
        }
    }
}