﻿using AutoMapper;
using EventPlannerServices.Api.Attributes;
using EventPlannerServices.Api.DTOs;
using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EventPlannerServices.Api.Controllers
{
    [Route("api/v{version:apiVersion}/roomtypes")]
    [ApiController]
    [ApiVersion("1.0")]
    public class RoomTypeController : ControllerBase
    {
        private readonly ILogger<RoomTypeController> _logger;
        private readonly IRoomTypeManager _roomTypeManager;
        private readonly IMapper _mapper;

        public RoomTypeController(ILogger<RoomTypeController> logger, IRoomTypeManager roomTypeManager, IMapper mapper)
        {
            _logger = logger;
            _roomTypeManager = roomTypeManager;
            _mapper = mapper;
        }

        [HttpGet]
        [MapToApiVersion("1.0")]
        public IActionResult GetAllRoomTypes()
        {
            try
            {
                var roomTypes = _roomTypeManager.GetAllRoomTypes();
                List<RoomTypeDto> roomTypeDtos = _mapper.Map<List<RoomTypeDto>>(roomTypes);

                return Ok(roomTypeDtos);
            }
            catch (Exception ex)
            {
                _logger.LogError("RoomTypeController.GetAllRoomTypes: Error getting all room types. Detail: {message}", ex.Message);
                return StatusCode(500, "Internal Server Error");
            }
        }
    }
}