﻿using AutoMapper;
using EventPlannerServices.Core.Interfaces.API;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EventPlannerServices.Api.Controllers
{
    [Route("api/v{version:apiVersion}/users")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    [ApiVersion("1.0")]
    public class UserController : ControllerBase
    {
        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;

        public UserController(IUserManager userManager, IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        [HttpDelete("delete/{userId}")]
        [MapToApiVersion("1.0")]
        public async Task<IActionResult> DeleteUser([FromRoute] int userId)
        {
            await _userManager.DeleteUser(userId);

            return Ok();
        }
    }
}