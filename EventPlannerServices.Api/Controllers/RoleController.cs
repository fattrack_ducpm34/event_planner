﻿using AutoMapper;
using EventPlannerServices.Api.DTOs;
using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EventPlannerServices.Api.Controllers
{
    [Route("api/v{version:apiVersion}/roles")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize]
    public class RoleController : Controller
    {
        private readonly IRoleManager _roleManger;
        private readonly IMapper _mapper;

        public RoleController(IRoleManager roleManager, IMapper mapper)
        {
            _roleManger = roleManager;
            _mapper = mapper;
        }

        [HttpPost("add-role")]
        [MapToApiVersion("1.0")]
        public async Task<IActionResult> CreateRole([FromBody] RoleDto roleDto)
        {
            var request = _mapper.Map<RoleDto, Role>(roleDto);

            await _roleManger.AddNewRole(request);

            return Ok();
        }
    }
}