﻿using AutoMapper;
using EventPlannerServices.Api.DTOs;
using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Request;

namespace EventPlannerServices.Api.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserDto, User>().ReverseMap();
            CreateMap<RoleDto, Role>().ReverseMap();
            CreateMap<ImageDto, Image>().ReverseMap();
            CreateMap<EmailDto, SendMailRequest>().ReverseMap();
            CreateMap<GetBookingInfoRequestDto, GetBookingInfoRequest>().ReverseMap();
            CreateMap<RoomType, RoomTypeDto>().ReverseMap();
            CreateMap<Room, RoomDTO>()
               .ForMember(dest => dest.RoomId, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.RoomCapacity, opt => opt.MapFrom(src => src.Capacity))
               .ForMember(dest => dest.RoomLocation, opt => opt.MapFrom(src => src.Location))
               .ForMember(dest => dest.RoomFloor, opt => opt.MapFrom(src => src.Floor))
               .ForMember(dest => dest.RoomName, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.RoomDescription, opt => opt.MapFrom(src => src.Description))
               .ForMember(dest => dest.RoomTypeId, opt => opt.MapFrom(src => src.RoomTypeId))
               .ForMember(dest => dest.RoomStatusId, opt => opt.MapFrom(src => src.StatusId))
               .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
               .ForMember(dest => dest.LastModifiedDate, opt => opt.MapFrom(src => src.LastModifiedDate))
               .ReverseMap();
        }
    }
}