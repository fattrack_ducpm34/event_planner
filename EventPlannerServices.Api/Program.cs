using EventPlannerServices.Api.Mapper;
using EventPlannerServices.Api.Middleware;
using EventPlannerServices.Api.Options;
using EventPlannerServices.Api.Redis;
using EventPlannerServices.Core.Entities;
using EventPlannerServices.Core.Interfaces.API;
using EventPlannerServices.Core.Interfaces.SPI;
using EventPlannerServices.Core.Mapper;
using EventPlannerServices.Core.Services;
using EventPlannerServices.Infrastructure.Database;
using EventPlannerServices.Infrastructure.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;

namespace EventPlannerServices.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder =>
                {
                    builder.AllowAnyOrigin()
                           .AllowAnyHeader()
                           .AllowAnyMethod();
                });
            });
            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Description = "Bearer Authentication with JWT Token",
                    Type = SecuritySchemeType.Http
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme
                            }
                        },
                        new List<string>()
                    }
                });
            });

            // API VERSIONING
            builder.Services.ConfigureOptions<ConfigureSwaggerOptions>();

            builder.Services.AddApiVersioning(opt =>
            {
                opt.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
                opt.AssumeDefaultVersionWhenUnspecified = true;
                opt.ReportApiVersions = true;
                opt.ApiVersionReader = new UrlSegmentApiVersionReader();
            });

            builder.Services.AddVersionedApiExplorer(setup =>
            {
                setup.GroupNameFormat = "'v'VVV";
                setup.SubstituteApiVersionInUrl = true;
            });

            builder.Services.AddDbContext<BookMeDbContext>(options =>
            {
                options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
            });

            // Redis
            builder.Services.InstallerServicesInAssembly(builder.Configuration);

            //Middleware
            builder.Services.AddTransient<GlobalExceptionHandlerMiddleware>();

            //Automapper
            builder.Services.AddAutoMapper(typeof(MappingProfile));
            builder.Services.AddAutoMapper(typeof(MappingResponseProfile));

            //GenericRepository
            builder.Services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));

            //Configure User's services
            builder.Services.AddScoped<IUserManager, UserManager>();
            builder.Services.AddScoped<IUserProvider, UserProvider>();

            //Configrue Role's services
            builder.Services.AddScoped<IRoleManager, RoleManager>();

            //Configure UserRole's services
            builder.Services.AddScoped<IUserRoleProvider, UserRoleProvider>();
            builder.Services.AddScoped<IUserRoleManager, UserRoleManager>();

            //Configure UserInfo's services
            builder.Services.AddScoped<IUserInfoManager, UserInfoManager>();
            builder.Services.AddScoped<IUserInfoProvider, UserInfoProvider>();

            //Configure Token's services
            builder.Services.AddScoped<ITokenService, TokenService>();

            //Configure Booking's service
            builder.Services.AddScoped<IBookingManager, BookingManager>();
            builder.Services.AddScoped<IBookingProvider, BookingProvider>();

            //Configure Room's service
            builder.Services.AddScoped<IRoomManager, RoomManager>();
            builder.Services.AddScoped<IRoomProvider, RoomProvider>();

            //Configure RoomType's service
            builder.Services.AddScoped<IRoomTypeManager, RoomTypeManager>();
            builder.Services.AddScoped<IRoomTypeProvider, RoomTypeProvider>();

            //Configure Email's service
            builder.Services.AddScoped<IEmailProvider, EmailProvider>();
            builder.Services.AddScoped<IEmailManager, EmailManager>();

            //Configure Image's service
            builder.Services.AddScoped<IImageProvider, ImageProvider>();
            builder.Services.AddScoped<IImageManager, ImageManager>();

            //Configure Authentication service
            builder.Services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ByYM000OLlMQG6VVVp1OH7Xzyr7gHuw1qvUC5dcGt3SNM")),
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ClockSkew = TimeSpan.Zero
                };
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {
                    var provider = app.Services.GetRequiredService<IApiVersionDescriptionProvider>();
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.ApiVersion.ToString());
                    }
                });
            }

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseMiddleware<GlobalExceptionHandlerMiddleware>();

            app.UseHttpsRedirection();

            app.UseCors("CorsPolicy");

            app.UseAuthentication();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}