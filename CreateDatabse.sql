USE master
GO
 
DROP DATABASE IF EXISTS BookMeDB
GO
 
CREATE DATABASE BookMeDB
GO
 
USE BookMeDB
GO
 
CREATE TABLE Roles(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Code NVARCHAR(100),
    Description NVARCHAR(100),
	CreatedDate DATETIME,
	LastModifiedDate DATETIME
)
 
CREATE TABLE "Users" (
    Id INT IDENTITY(1,1) PRIMARY KEY,
    Username NVARCHAR(100),
    Fullname NVARCHAR(100),
    Email NVARCHAR(100),
	"Password" NVARCHAR(100),
	Token NVARCHAR(MAX),
	RefreshToken NVARCHAR(MAX),
	RefreshTokenExpiryTime DATETIME,
	CreatedDate DATETIME,
	LastModifiedDate DATETIME
);
 
CREATE TABLE UserRole (
    Id INT IDENTITY(1,1) PRIMARY KEY,
    UserId INT NOT NULL,
    RoleId INT NOT NULL,
	CreatedDate DATETIME,
	LastModifiedDate DATETIME,
    FOREIGN KEY (UserId) REFERENCES "Users"(Id),
    FOREIGN KEY (RoleId) REFERENCES Roles(Id)
);
 
CREATE TABLE RoomTypes(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name NVARCHAR(1000),
	Description NVARCHAR(1000),
	CreatedDate DATETIME,
	LastModifiedDate DATETIME
)
 
CREATE TABLE Images(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name VARCHAR(1000),
	ImageData VARBINARY(MAX),
	CreatedDate DATETIME,
	LastModifiedDate DATETIME
)
 
CREATE TABLE RoomStatus(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name NVARCHAR(1000),
	Description NVARCHAR(1000),
	CreatedDate DATETIME,
	LastModifiedDate DATETIME
)
 
CREATE TABLE Rooms(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Capacity INT,
	Location NVARCHAR(1000),
	Floor INT,
	Name NVARCHAR(1000),
	Description NVARCHAR(1000),
	RoomTypeID INT,
	StatusID INT,
	IsDelete BIT,
	CreatedDate DATETIME,
	LastModifiedDate DATETIME
	FOREIGN KEY (RoomTypeID) REFERENCES RoomTypes(Id),
	FOREIGN KEY (StatusID) REFERENCES RoomStatus(Id)
)
 
CREATE TABLE ImageMap(
	Id INT PRIMARY KEY IDENTITY(1,1),
	ImageID INT,
	RoomID INT,
	FOREIGN KEY (RoomID) REFERENCES Rooms(Id),
	FOREIGN KEY (ImageID) REFERENCES Images(Id)
)
 
CREATE TABLE Services(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name NVARCHAR(1000),
	Description NVARCHAR(1000),
	Price INT,
	IsDelete BIT,
	CreatedDate DATETIME,
	LastModifiedDate DATETIME
)
 
CREATE TABLE AttendeeStatus(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name NVARCHAR(1000),
	Description NVARCHAR(1000),
	CreatedDate DATETIME,
	LastModifiedDate DATETIME
)
 
CREATE TABLE Booking(
	Id INT PRIMARY KEY IDENTITY(1,1),
	UserID INT,
	RoomID INT,
	Name NVARCHAR(1000),
	Description NVARCHAR(1000),
	StartTime DATETIME,
	EndTime DATETIME,
	AttendeesList NVARCHAR(1000),
	IsDelete BIT,
	CreatedDate DATETIME,
	LastModifiedDate DATETIME
	FOREIGN KEY (UserID) REFERENCES Users(Id),
	FOREIGN KEY (RoomID) REFERENCES Rooms(Id)
)
 
CREATE TABLE Attendees(
	Id INT PRIMARY KEY IDENTITY(1,1),
	BookingID INT,
	Email VARCHAR(1000),
	AttendeeStatusID INT,
	CreatedDate DATETIME,
	LastModifiedDate DATETIME,
	FOREIGN KEY (BookingID) REFERENCES Booking(Id),
	FOREIGN KEY (AttendeeStatusID) REFERENCES AttendeeStatus(Id)
)
 
CREATE TABLE Booking_Service(
	BookingID INT,
	ServiceID INT,
	IsPaid BIT,
	IsDelete BIT,
	CreatedDate DATETIME,
	LastModifiedDate DATETIME
	PRIMARY KEY (BookingID,ServiceID),
	FOREIGN KEY (BookingID) REFERENCES Booking(Id),
	FOREIGN KEY (ServiceID) REFERENCES Services(Id)
)

-- Insert data for roles table
INSERT INTO Roles (Code,"Description")
VALUES ('admin','administrator')
INSERT INTO Roles (Code,"Description")
VALUES('user','user')
SELECT * FROM Roles

-- Insert data for Services tables
INSERT INTO Services (Name, Price, IsDelete)
VALUES('Water',50000,0)
INSERT INTO Services (Name, Price, IsDelete)
VALUES('Fruice',50000,0)
SELECT * FROM Services

-- Insert data for Users table 
INSERT INTO Users (Email, Username, Password)
VALUES('ThangTD13@fpt.com','ThangTD13','beNNmT2+CaJsJlinACfCby5aiHIbPYMxzBxHmETlF4IZTMcd')
INSERT INTO Users (Email, Username, Password)
VALUES('DucPM34@fpt.com','DucPM34','beNNmT2+CaJsJlinACfCby5aiHIbPYMxzBxHmETlF4IZTMcd')
INSERT INTO Users (Email, Username, Password)
VALUES('ThangLX4@fpt.com','ThangLX4','beNNmT2+CaJsJlinACfCby5aiHIbPYMxzBxHmETlF4IZTMcd')
INSERT INTO Users (Email, Username, Password)
VALUES('ThanhHT18@fpt.com','ThanhHT18','beNNmT2+CaJsJlinACfCby5aiHIbPYMxzBxHmETlF4IZTMcd')
INSERT INTO Users (Email, Username, Password)
VALUES('admin@identity.fpt.com','admin','beNNmT2+CaJsJlinACfCby5aiHIbPYMxzBxHmETlF4IZTMcd')
SELECT * FROM Users

-- Insert data for RoomStatus table
INSERT INTO RoomStatus (Name, Description)
VALUES('can order', 'ready for book')
INSERT INTO RoomStatus (Name, Description)
VALUES('out of order', 'room in maintain')
SELECT * FROM RoomStatus

-- Insert data for RoomTypes table
INSERT INTO RoomTypes (Name, Description)
VALUES('meeting room', 'meeting room')
INSERT INTO RoomTypes (Name, Description)
VALUES('large metting room','large metting room')
INSERT INTO RoomTypes (Name, Description)
VALUES('football pitches','football pitches')
INSERT INTO RoomTypes (Name, Description)
VALUES('banquet hall','banquet hall')
INSERT INTO RoomTypes (Name, Description)
VALUES('swimming pool','swimming pool')
SELECT * FROM RoomTypes

-- Insert data for AttendeeStatus table
INSERT INTO AttendeeStatus (Name, Description)
VALUES('attending','attending description')
INSERT INTO AttendeeStatus (Name, Description)
VALUES('not attending','not attending description')
INSERT INTO AttendeeStatus (Name, Description)
VALUES('not yet response','not yet response description')
SELECT * FROM AttendeeStatus

-- Insert data for Rooms table
INSERT INTO Rooms (Capacity, Location, Floor, Name, Description, IsDelete, RoomTypeID, StatusID)
VALUES(10,'17 Duy Tan',10,'Paris','Paris description',0,1,1)
INSERT INTO Rooms (Capacity, Location, Floor, Name, Description, IsDelete, RoomTypeID, StatusID)
VALUES(8,'17 Duy Tan',10,'Passion','Passion description',0,1,1)
INSERT INTO Rooms (Capacity, Location, Floor, Name, Description, IsDelete, RoomTypeID, StatusID)
VALUES(8,'17 Duy Tan',12,'Happy','Happy description',0,1,1)
INSERT INTO Rooms (Capacity, Location, Floor, Name, Description, IsDelete, RoomTypeID, StatusID)
VALUES(100,'F villie 1, KCNC, Hoa Lac, Thach That',0,'FV1 simming pool','swimming pool',0,5,1)
INSERT INTO Rooms (Capacity, Location, Floor, Name, Description, IsDelete, RoomTypeID, StatusID)
VALUES(20,'10 Pham Van Bach',5,'Mon Soon','Mon Soon description',0,1,1)
INSERT INTO Rooms (Capacity, Location, Floor, Name, Description, IsDelete, RoomTypeID, StatusID)
VALUES(26,'10 Pham Van Bach',10,'Bangkok','VIP metting room',0,1,1)
INSERT INTO Rooms (Capacity, Location, Floor, Name, Description, IsDelete, RoomTypeID, StatusID)
VALUES(12,'10 Pham Van Bach',12,'SaiGon','SaiGon description',0,1,1)
INSERT INTO Rooms (Capacity, Location, Floor, Name, Description, IsDelete, RoomTypeID, StatusID)
VALUES(300,'489 Hoang Quoc Viet, Co Nhue, Bac Tu Liem, Ha Noi',12,'Trong Dong banquet hall','description',0,4,1)
INSERT INTO Rooms (Capacity, Location, Floor, Name, Description, IsDelete, RoomTypeID, StatusID)
VALUES(1000,'F villie 1, KCNC, Hoa Lac, Thach That',0,'FV1 football pitch','football pitch',0,3,1)
INSERT INTO Rooms (Capacity, Location, Floor, Name, Description, IsDelete, RoomTypeID, StatusID)
VALUES(500,'Urban Area, Keangnam Hanoi Landmark Tower, Plot E6, Keangnam, Cau Giay, Ha Noi',5,'intercontinental restaurant','kaengnam tower',0,4,1)
SELECT * FROM Rooms

-- Insert data for Booking table
INSERT INTO Booking (UserID, RoomID, Name, Description, StartTime, EndTime, IsDelete)
VALUES('1',1,'Sitecore daily metting','Sitecore team daily meeting','2024-01-08 09:00:00','2024-01-08 09:30:00',0)
INSERT INTO Booking (UserID, RoomID, Name, Description, StartTime, EndTime, IsDelete)
VALUES('2',2,'Hydra customer demo project','Hydra demo project','2024-01-08 09:00:00','2024-01-08 10:30:00',0)
INSERT INTO Booking (UserID, RoomID, Name, Description, StartTime, EndTime, IsDelete)
VALUES('1',10,'GIS year end party','EYP','2024-01-24 18:00:00','2024-01-24 22:00:00',0)
INSERT INTO Booking (UserID, RoomID, Name, Description, StartTime, EndTime, IsDelete)
VALUES('3',9,'FHL footbal','EYP','2024-01-26 08:00:00','2024-01-26 14:00:00',0)
INSERT INTO Booking (UserID, RoomID, Name, Description, StartTime, EndTime, IsDelete)
VALUES('4',6,'Kickoff project 2024','kickoff project','2024-01-22 10:00:00','2024-01-22 11:00:00',0)
INSERT INTO Booking (UserID, RoomID, Name, Description, StartTime, EndTime, IsDelete)
VALUES('2',2,'Ceberus sprint planning','sprint planning','2024-01-22 14:00:00','2024-01-22 15:30:00',0)
SELECT * FROM Booking

-- Insert data for Booking_Service table
INSERT INTO Booking_Service (BookingID, ServiceID, IsPaid, IsDelete)
VALUES (1,1,0,0)
INSERT INTO Booking_Service (BookingID, ServiceID, IsPaid, IsDelete)
VALUES (1,2,0,0)
SELECT * FROM Booking_Service


-- Functions & Store procedure
-- Function check room is conflict
GO
CREATE FUNCTION dbo.CheckConflictRoom(@StartTime DATETIME, @EndTime DATETIME, @RoomID INT)
RETURNS BIT
AS
BEGIN
	RETURN (SELECT CASE WHEN EXISTS (
	SELECT *
	FROM Booking
	WHERE StartTime < @EndTime AND EndTime > @StartTime AND RoomID = @RoomID AND IsDelete = 0
)
THEN CAST(1 AS BIT)
ELSE CAST (0 AS BIT) END)
END

-- Store procedure insert booking
GO
CREATE PROCEDURE InsertBooking @UserID INT, @RoomID INT,@StartTime DATETIME, @EndTime DATETIME, @Name NVARCHAR(1000), @Description NVARCHAR(1000), @AttendeesList NVARCHAR(1000)
AS
BEGIN
    IF(@StartTime >= @EndTime)
    BEGIN
        SELECT 'End Time must be greater than Start Time'
    END
    ELSE IF(@StartTime <= GETDATE())
    BEGIN
        SELECT 'Start Time must be greater than now'
    END
    ELSE IF(@EndTime <= GETDATE())
    BEGIN
        SELECT 'End Time must be greater than now'
    END
    ELSE IF((SELECT dbo.CheckConflictRoom(@StartTime, @EndTime, @RoomID)) = 1)
    BEGIN
        SELECT 'Room is booked'
    END
    ELSE
    BEGIN
        INSERT INTO Booking (UserID, RoomID, Name, Description, StartTime, EndTime, IsDelete, AttendeesList, CreatedDate)
        VALUES(@UserID, @RoomID, @Name, @Description, @StartTime, @EndTime, 0, @AttendeesList, GETDATE());
    END
END
GO

EXEC InsertBooking @UserID = 2, @RoomID = 1, @StartTime = '2024-01-11 09:30:00', @EndTime = '2024-01-11 09:31:00', @Name = 'Thang booking', @Description = 'Thang booking description', @AttendeesList = 'ThangTD13@fpt.com'
SELECT * FROM Booking

--Create index
GO
CREATE INDEX room_location_index 
ON Rooms (Location)

GO
CREATE INDEX user_username_index
ON Users (Username)

GO
CREATE INDEX rooms_name_index
ON Rooms (Name)

GO
CREATE INDEX rooms_capacity_index
ON Rooms (Capacity)